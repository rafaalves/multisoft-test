import 'dart:convert';

import 'json_reader.dart';

enum JsonPath {
  breedListPath("helpers/stubs/breed_list.json"),
  ;

  const JsonPath(this.path);

  final String path;

  Map<String, dynamic> get json {
    final json = jsonDecode(jsonReader(path));
    if (json is List) {
      return {"data": json};
    }
    return json;
  }
}
