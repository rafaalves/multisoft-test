import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/dependencies_injection.dart';
import 'package:multisoft_test/features/breeds/breeds.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

import '../../../../helpers/fake_path_provider_platform.dart';
import '../../../../helpers/paths.dart';
import '../../../../helpers/test_mock.mocks.dart';

void main() {
  late BreedsCubit cubit;
  late MockGetBreedsUsecase usecase;
  late BreedsListEntity breeds;

  const dummyBreedsRequest1 = BreedsParams();
  const dummyBreedsRequest2 = BreedsParams(page: 2);
  const errorMessage = "";

  /// Initialize data
  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProvider();
    await serviceLocator(isUnitTest: true, prefixBox: 'breeds_cubit_test_');

    breeds = BreedsListResponse.fromJson(JsonPath.breedListPath.json).toEntity();
    usecase = MockGetBreedsUsecase();
    cubit = BreedsCubit(usecase);
  });

  /// Dispose bloc
  tearDown(() {
    cubit.close();
  });

  ///  Initial data should be loading
  test("Initial data should be BreedsStatus.loading", () {
    expect(cubit.state, const BreedsState.loading());
  });

  blocTest<BreedsCubit, BreedsState>(
    "When repo success get data should be return BreedsState and loading only show when request page 0",
    build: () {
      when(usecase.call(dummyBreedsRequest1)).thenAnswer((_) async => Right(breeds));

      return cubit;
    },
    act: (BreedsCubit breedsCubit) => breedsCubit.fetch(dummyBreedsRequest1),
    wait: const Duration(milliseconds: 100),
    expect: () => [
      const BreedsState.loading(),
      BreedsState.success(breeds.data ?? []),
    ],
  );

  blocTest<BreedsCubit, BreedsState>(
    "When request page 2, isLoading should not execute",
    build: () {
      when(usecase.call(dummyBreedsRequest2)).thenAnswer((_) async => Right(breeds));

      return cubit;
    },
    act: (BreedsCubit breedsCubit) => breedsCubit.fetch(dummyBreedsRequest2),
    wait: const Duration(milliseconds: 100),
    expect: () => [BreedsState.success(breeds.data ?? [])],
  );

  blocTest<BreedsCubit, BreedsState>(
    "When failed to get data from server",
    build: () {
      when(
        usecase.call(dummyBreedsRequest1),
      ).thenAnswer((_) async => const Left(ServerFailure(message: errorMessage)));

      return BreedsCubit(usecase);
    },
    act: (BreedsCubit breedsCubit) => breedsCubit.fetch(dummyBreedsRequest1),
    wait: const Duration(milliseconds: 100),
    expect: () => const [
      BreedsState.loading(),
      BreedsState.failure(errorMessage),
    ],
  );

  blocTest<BreedsCubit, BreedsState>(
    "When no data from server",
    build: () {
      when(usecase.call(dummyBreedsRequest2)).thenAnswer((_) async => Left(NoDataFailure()));

      return BreedsCubit(usecase);
    },
    act: (BreedsCubit breedsCubit) => breedsCubit.fetch(dummyBreedsRequest2),
    wait: const Duration(milliseconds: 100),
    expect: () => [
      const BreedsState.success([]),
    ],
  );

  blocTest<BreedsCubit, BreedsState>(
    "When request refreshBreeds",
    build: () {
      when(usecase.call(dummyBreedsRequest1)).thenAnswer((_) async => Right(breeds));

      return BreedsCubit(usecase);
    },
    act: (BreedsCubit breedsCubit) => breedsCubit.refresh(dummyBreedsRequest1),
    wait: const Duration(milliseconds: 100),
    expect: () => [
      const BreedsState.loading(),
      BreedsState.success(breeds.data ?? []),
    ],
  );
}
