import 'dart:io';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/dependencies_injection.dart';
import 'package:multisoft_test/features/breeds/breeds.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

import '../../../../helpers/fake_path_provider_platform.dart';
import '../../../../helpers/paths.dart';
import '../../../../helpers/test_mock.mocks.dart';

class MockBreedsCubit extends MockCubit<BreedsState> implements BreedsCubit {}

class FakeBreedState extends Fake implements BreedsState {}

void main() {
  late BreedsCubit cubit;
  late BreedsListEntity breeds;

  setUpAll(() {
    HttpOverrides.global = null;
    registerFallbackValue(FakeBreedState());
    registerFallbackValue(const BreedsParams());
  });

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProvider();
    await serviceLocator(isUnitTest: true, prefixBox: 'breed_list_page_test_');
    cubit = MockBreedsCubit();
    breeds = BreedsListResponse.fromJson(JsonPath.breedListPath.json).toEntity();
  });

  Widget rootWidget(Widget body) {
    return BlocProvider<BreedsCubit>.value(
      value: cubit,
      child: MaterialApp(
        localizationsDelegates: const [
          Strings.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        locale: const Locale("en"),
        theme: themeLight(MockBuildContext()),
        home: body,
      ),
    );
  }

  testWidgets('renders BreedListPage for BreedsStatus.loading', (tester) async {
    when(() => cubit.state).thenReturn(const BreedsState.loading());
    await tester.pumpWidget(rootWidget(const BreedListPage()));
    await tester.pump();
    expect(find.byType(AppLoading), findsOneWidget);
  });

  testWidgets('renders BreedListPage for BreedsStatus.empty', (tester) async {
    when(() => cubit.state).thenReturn(const BreedsState.empty());
    await tester.pumpWidget(rootWidget(const BreedListPage()));
    await tester.pump();
    expect(find.byType(AppEmpty), findsOneWidget);
  });

  testWidgets('renders BreedListPage for BreedsStatus.failure', (tester) async {
    when(() => cubit.state).thenReturn(const BreedsState.failure(""));
    await tester.pumpWidget(rootWidget(const BreedListPage()));
    await tester.pump();
    expect(find.byType(AppEmpty), findsOneWidget);
  });

  testWidgets('renders BreedListPage for BreedsStatus.success', (tester) async {
    when(() => cubit.state).thenReturn(
      BreedsState.success(breeds.data ?? []),
    );
    await tester.pumpWidget(rootWidget(const BreedListPage()));
    await tester.pump();

    expect(find.byType(ListView), findsOneWidget);
  });

  testWidgets('trigger refresh when pull to refresh', (tester) async {
    when(() => cubit.state).thenReturn(
      BreedsState.success(breeds.data ?? []),
    );
    when(() => cubit.refresh(any())).thenAnswer((_) async {});

    await tester.pumpWidget(rootWidget(const BreedListPage()));
    await tester.pump();
    await tester.fling(
      find.text('Abyssinian'),
      const Offset(0.0, 500.0),
      1000.0,
    );

    /// Do loops to waiting refresh indicator showing
    /// instead using tester.pumpAndSettle it's will result time out error
    for (int i = 0; i < 5; i++) {
      await tester.pump(const Duration(milliseconds: 100));
    }
    verify(() => cubit.refresh(any())).called(1);
  });
}
