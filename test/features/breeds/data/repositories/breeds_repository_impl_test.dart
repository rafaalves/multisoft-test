import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/dependencies_injection.dart';
import 'package:multisoft_test/features/breeds/breeds.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

import '../../../../helpers/fake_path_provider_platform.dart';
import '../../../../helpers/paths.dart';
import '../../../../helpers/test_mock.mocks.dart';

void main() {
  late MockIBreedsRemoteDatasource datasource;
  late BreedsRepositoryImpl repository;
  late BreedsListEntity breeds;

  setUp(() async {
    TestWidgetsFlutterBinding.ensureInitialized();
    PathProviderPlatform.instance = FakePathProvider();
    await serviceLocator(
      isUnitTest: true,
      prefixBox: 'breeds_repository_impl_test_',
    );
    datasource = MockIBreedsRemoteDatasource();
    repository = BreedsRepositoryImpl(datasource);
    breeds = BreedsListResponse.fromJson(JsonPath.breedListPath.json).toEntity();
  });

  group("breed", () {
    const params = BreedsParams();
    const paramsFailure = BreedsParams(page: 100);
    const paramsEmpty = BreedsParams(page: 3);

    test('should return list breed when call data is successful', () async {
      // arrange
      when(datasource.list(params)).thenAnswer(
        (_) async => Right(
          BreedsListResponse.fromJson(JsonPath.breedListPath.json),
        ),
      );

      // act
      final result = await repository.getBreeds(params);

      // assert
      verify(datasource.list(params));
      expect(result, equals(Right(breeds)));
    });

    test('should return empty list breed when call data is successful', () async {
      // arrange
      when(datasource.list(paramsEmpty)).thenAnswer(
        (_) async => Left(NoDataFailure()),
      );

      // act
      final result = await repository.getBreeds(paramsEmpty);

      // assert
      verify(datasource.list(paramsEmpty));
      expect(result, equals(Left(NoDataFailure())));
    });

    test('should return server failure when call data is unsuccessful', () async {
      // arrange
      when(datasource.list(paramsFailure))
          .thenAnswer((_) async => const Left(ServerFailure(message: '')));

      // act
      final result = await repository.getBreeds(paramsFailure);

      // assert
      verify(datasource.list(paramsFailure));
      expect(result, equals(const Left(ServerFailure(message: ''))));
    });
  });
}
