import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:multisoft_test/features/breeds/breeds.dart';

import '../../../../helpers/paths.dart';
import '../../../../helpers/test_mock.mocks.dart';

void main() {
  late MockIBreedsRepository repository;
  late GetBreedsUsecase usecase;
  late BreedsListEntity breeds;
  const params = BreedsParams();

  setUp(() {
    breeds = BreedsListResponse.fromJson(JsonPath.breedListPath.json).toEntity();
    repository = MockIBreedsRepository();
    usecase = GetBreedsUsecase(repository);
  });

  test("should get breeds from the repository", () async {
    /// arrange
    when(repository.getBreeds(params)).thenAnswer((_) async => Right(breeds));

    /// act
    final result = await usecase.call(params);

    /// assert
    expect(result, equals(Right(breeds)));
  });
}
