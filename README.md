# Multisoft Test App

<br>This app also implementing **Flutter Clean Architecture.**

## Pre-requisites

| Technology | Recommended Version | Installation Guide                                                    |
|------------|---------------------|-----------------------------------------------------------------------|
| Flutter    | v3.10.x             | [Flutter Official Docs](https://flutter.dev/docs/get-started/install) |
| Dart       | v3.0.x              | Installed automatically with Flutter                                  |

## Getting Started

- Clone this project
- Run `flutter pub get`
- Run `flutter gen-l10n` to generate localization files
- Run `dart run build_runner build --delete-conflicting-outputs` to generate freezes files
- Run Test `flutter test`

## Feature

- [x] BLoC State Management
- [x] **Clean Architecture**
    - [x] Unit Test
    - [x] Widget Test
    - [x] BLoC test
- [x] Theme Configuration: `System, Light, Dark`
- [x] Multi-Language: `English, Português`
- [x] Routing System using [Go Router](https://pub.dev/packages/go_router)
- [x] Dependency Injection with [Get It](https://pub.dev/packages/get_it)
- [x] Gitlab CI to build, test, and deploy apps to [App Center](https://appcenter.ms/)

## Todo

- [ ] Implement multi-flavor
- [ ] Integration Test
- [ ] Adaptive layout using [ScreenUtil](https://pub.dev/packages/flutter_screenutil)
- [ ] Apply rules to build, test, and deploy to Gitlab CI 


## Architecture Proposal

<br>

![architecture-proposal](./architecture-proposal.png)