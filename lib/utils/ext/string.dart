import 'package:flutter/material.dart';
import 'package:multisoft_test/core/core.dart';

extension StringExtension on String {
  bool isValidEmail() {
    return RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$',
    ).hasMatch(this);
  }

  void toToastError(BuildContext context) {
    final message = isEmpty ? "error" : this;
    AppToast.error(context, message);
  }

  void toToastSuccess(BuildContext context) {
    final message = isEmpty ? "success" : this;
    AppToast.success(context, message);
  }

  void toToastLoading(BuildContext context) {
    final message = isEmpty ? "loading" : this;
    AppToast.alert(context, message);
  }
}
