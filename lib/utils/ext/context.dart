import 'package:flutter/material.dart';
import 'package:multisoft_test/core/core.dart';

extension ContextExtensions on BuildContext {
  bool isMobile() {
    final shortestSide = MediaQuery.of(this).size.shortestSide;
    return shortestSide < 600;
  }

  double widthInPercent(double percent) {
    final toDouble = percent / 100;
    return MediaQuery.of(this).size.width * toDouble;
  }

  double heightInPercent(double percent) {
    final toDouble = percent / 100;
    return MediaQuery.of(this).size.height * toDouble;
  }

  static late BuildContext ctx;

  Future<void> show() {
    return showDialog(
      context: this,
      barrierDismissible: false,
      builder: (c) {
        ctx = c;

        return PopScope(
          canPop: false,
          child: Material(
            color: Colors.transparent,
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(this).extension<AppColors>()!.background,
                  borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                ),
                margin: EdgeInsets.symmetric(horizontal: Dimens.space30),
                padding: EdgeInsets.all(Dimens.space24),
                child: const AppLoading(),
              ),
            ),
          ),
        );
      },
    );
  }

  void dismiss() {
    try {
      Navigator.pop(ctx);
    } catch (_) {}
  }

  Future<void> openDangerAlert({
    String? title,
    String? positiveLabelButton,
    required String description,
    required VoidCallback onPressed,
  }) {
    return AppAlert.showDanger(
      context: this,
      title: title,
      description: description,
      onPressed: onPressed,
      positiveLabelButton: positiveLabelButton,
    );
  }
}
