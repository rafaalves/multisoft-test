class Constants {
  Constants._();

  static Constants get = Constants._();

  String appName = "Pet List";

  static String defaultLanguage = portugueseAbbreviated;
  static String english = "English";
  static String englishAbbreviated = "en";
  static String portuguese = "Português";
  static String portugueseAbbreviated = "pt";

  String primaryFontFamily = "Poppins";
}
