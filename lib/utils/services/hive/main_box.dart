import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

import '../../helper/helper.dart';

enum ActiveTheme {
  light(ThemeMode.light),
  dark(ThemeMode.dark),
  system(ThemeMode.system);

  final ThemeMode mode;

  const ActiveTheme(this.mode);
}

enum MainBoxKeys {
  token,
  fcm,
  language,
  theme,
  locale,
  isLogin,
}

mixin class MainBoxMixin {
  static late Box? mainBox;
  static late CollectionBox<Map> petBox;
  static late CollectionBox<Map> breedBox;

  static const _boxName = 'multisoft_app';
  static const _collectionName = '${_boxName}_collection';
  static const _petBoxName = 'pet_db';
  static const _breedBoxName = 'breed_db';

  static Future<void> initHive(String prefixBox) async {
    await Hive.initFlutter();
    mainBox = await Hive.openBox("$prefixBox$_boxName");

    try {
      // Create a box collection
      final directory = await getApplicationDocumentsDirectory();
      final collection = await BoxCollection.open(
        "$prefixBox$_collectionName",
        {_petBoxName, _breedBoxName},
        path: directory.path,
      );

      // Open your boxes. Optional: Give it a type.
      petBox = await collection.openBox<Map>(_petBoxName);
      breedBox = await collection.openBox<Map>(_breedBoxName);

    } catch (err) {
      log.e(err.toString(), error: err);
    }
  }

  Future<void> addData<T>(MainBoxKeys key, T value) async {
    await mainBox?.put(key.name, value);
  }

  Future<void> removeData(MainBoxKeys key) async {
    await mainBox?.delete(key.name);
  }

  T getData<T>(MainBoxKeys key) => mainBox?.get(key.name) as T;

  Future<void> logoutBox() async {
    /// Clear the box
    removeData(MainBoxKeys.isLogin);
    removeData(MainBoxKeys.token);
  }

  Future<void> closeBox({bool isUnitTest = false}) async {
    try {
      if (mainBox != null) {
        await mainBox?.close();
        await mainBox?.deleteFromDisk();
      }
    } catch (e) {
      if (!isUnitTest) {
        // FirebaseCrashLogger().nonFatalError(error: e, stackTrace: stackTrace);
      }
    }
  }

  Future<void> putCacheBreeds(String key, Map response) async {
    await breedBox.put(key, response);
  }

  Future<Map<String, dynamic>?> getCacheBreeds(String key) async {
    final map = await MainBoxMixin.breedBox.get(key);
    if (map == null) {
      return null;
    }
    return jsonDecode(jsonEncode(map ?? {}));
  }
}
