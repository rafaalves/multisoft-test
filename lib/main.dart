import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multisoft_test/dependencies_injection.dart';
import 'package:multisoft_test/my_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await serviceLocator();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(const MyApp());
}