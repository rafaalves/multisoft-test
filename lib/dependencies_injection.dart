import 'package:get_it/get_it.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/features/breeds/breeds.dart';
import 'package:multisoft_test/features/pets/pets.dart';
import 'package:multisoft_test/utils/utils.dart';

GetIt sl = GetIt.instance;

Future<void> serviceLocator({
  bool isUnitTest = false,
  bool isHiveEnable = true,
  String prefixBox = '',
}) async {
  /// For unit testing only
  if (isUnitTest) {
    await sl.reset();
  }

  sl.registerSingleton<ApiClient>(ApiClient(isUnitTest: isUnitTest));

  _breeds();
  _pets();

  if (isHiveEnable) {
    await _initHiveBoxes(
      isUnitTest: isUnitTest,
      prefixBox: prefixBox,
    );
  }
}

Future<void> _initHiveBoxes({
  bool isUnitTest = false,
  String prefixBox = '',
}) async {
  await MainBoxMixin.initHive(prefixBox);
  sl.registerSingleton<MainBoxMixin>(MainBoxMixin());
}

void _breeds() {
  /// Datasources
  sl.registerLazySingleton<IBreedsRemoteDatasource>(() => BreedsRemoteDatasourceImpl(sl()));

  /// Repositories
  sl.registerLazySingleton<IBreedsRepository>(() => BreedsRepositoryImpl(sl()));

  /// Usecases
  sl.registerLazySingleton(() => GetBreedsUsecase(sl()));

  /// Cubits
  sl.registerFactory<BreedsCubit>(() => BreedsCubit(sl()));
}

void _pets() {
  /// Datasources
  sl.registerLazySingleton<IPetsRemoteDatasource>(() => PetsRemoteDatasourceImp());

  /// Repositories
  sl.registerLazySingleton<IPetsRepository>(() => PetsRepositoryImpl(sl()));

  /// Usecases
  sl.registerLazySingleton(() => SavePetUsecase(sl()));
  sl.registerLazySingleton(() => DeletePetUsecase(sl()));
  sl.registerLazySingleton(() => GetPetsUsecase(sl()));

  /// Cubits
  sl.registerFactory<ListPetCubit>(() => ListPetCubit(sl()));
  sl.registerFactory<EditPetCubit>(() => EditPetCubit(sl()));
  sl.registerFactory<DeletePetCubit>(() => DeletePetCubit(sl()));
}
