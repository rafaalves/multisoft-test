import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../entities/entities.dart';

abstract class IPetsRepository {
  Future<Either<IFailure, PetsListEntity>> getList(ListParams params);
  Future<Either<IFailure, bool>> save(SaveParams params);
  Future<Either<IFailure, bool>> delete(DeleteParams params);
}
