import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../entities/entities.dart';
import '../repositories/repositories.dart';

class GetPetsUsecase extends Usecase<PetsListEntity, ListParams> {
  final IPetsRepository _repo;

  GetPetsUsecase(this._repo);

  @override
  Future<Either<IFailure, PetsListEntity>> call(ListParams params) => _repo.getList(params);
}
