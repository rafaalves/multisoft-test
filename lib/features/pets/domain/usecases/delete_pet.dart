import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../entities/entities.dart';
import '../repositories/repositories.dart';

class DeletePetUsecase extends Usecase<bool, DeleteParams> {
  final IPetsRepository _repo;

  DeletePetUsecase(this._repo);

  @override
  Future<Either<IFailure, bool>> call(DeleteParams params) async {
    return _repo.delete(params);
  }
}
