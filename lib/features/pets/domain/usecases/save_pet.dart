import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../entities/entities.dart';
import '../repositories/repositories.dart';

class SavePetUsecase extends Usecase<bool, SaveParams> {
  final IPetsRepository _repo;

  SavePetUsecase(this._repo);

  @override
  Future<Either<IFailure, bool>> call(SaveParams params) async {
    return _repo.save(params);
  }
}
