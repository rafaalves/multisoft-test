class DeleteParams {
  final String id;

  const DeleteParams({
    required this.id,
  });
}
