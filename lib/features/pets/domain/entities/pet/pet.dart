import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/features/breeds/domain/entities/breed/breed.dart';

part 'pet.freezed.dart';
part 'pet.g.dart';

@freezed
class PetsListEntity with _$PetsListEntity {
  const factory PetsListEntity({
    List<PetEntity>? data,
    int? currentPage,
    int? lastPage,
  }) = _PetsListEntity;
}

@freezed
class PetEntity extends Equatable with _$PetEntity {
  const PetEntity._();

  const factory PetEntity({
    String? id,
    String? name,
    String? description,
    BreedEntity? breed,
  }) = _PetEntity;

  factory PetEntity.fromJson(Map<String, dynamic> json) => _$PetEntityFromJson(json);

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        breed,
      ];
}
