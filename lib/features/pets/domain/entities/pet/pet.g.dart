// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pet.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PetEntityImpl _$$PetEntityImplFromJson(Map<String, dynamic> json) =>
    _$PetEntityImpl(
      id: json['id'] as String?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      breed: json['breed'] == null
          ? null
          : BreedEntity.fromJson(json['breed'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$PetEntityImplToJson(_$PetEntityImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'breed': instance.breed?.toJson(),
    };
