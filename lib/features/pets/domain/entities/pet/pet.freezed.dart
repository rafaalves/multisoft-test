// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pet.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PetsListEntity {
  List<PetEntity>? get data => throw _privateConstructorUsedError;
  int? get currentPage => throw _privateConstructorUsedError;
  int? get lastPage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PetsListEntityCopyWith<PetsListEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PetsListEntityCopyWith<$Res> {
  factory $PetsListEntityCopyWith(
          PetsListEntity value, $Res Function(PetsListEntity) then) =
      _$PetsListEntityCopyWithImpl<$Res, PetsListEntity>;
  @useResult
  $Res call({List<PetEntity>? data, int? currentPage, int? lastPage});
}

/// @nodoc
class _$PetsListEntityCopyWithImpl<$Res, $Val extends PetsListEntity>
    implements $PetsListEntityCopyWith<$Res> {
  _$PetsListEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
    Object? currentPage = freezed,
    Object? lastPage = freezed,
  }) {
    return _then(_value.copyWith(
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<PetEntity>?,
      currentPage: freezed == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int?,
      lastPage: freezed == lastPage
          ? _value.lastPage
          : lastPage // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PetsListEntityImplCopyWith<$Res>
    implements $PetsListEntityCopyWith<$Res> {
  factory _$$PetsListEntityImplCopyWith(_$PetsListEntityImpl value,
          $Res Function(_$PetsListEntityImpl) then) =
      __$$PetsListEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<PetEntity>? data, int? currentPage, int? lastPage});
}

/// @nodoc
class __$$PetsListEntityImplCopyWithImpl<$Res>
    extends _$PetsListEntityCopyWithImpl<$Res, _$PetsListEntityImpl>
    implements _$$PetsListEntityImplCopyWith<$Res> {
  __$$PetsListEntityImplCopyWithImpl(
      _$PetsListEntityImpl _value, $Res Function(_$PetsListEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
    Object? currentPage = freezed,
    Object? lastPage = freezed,
  }) {
    return _then(_$PetsListEntityImpl(
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<PetEntity>?,
      currentPage: freezed == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int?,
      lastPage: freezed == lastPage
          ? _value.lastPage
          : lastPage // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$PetsListEntityImpl implements _PetsListEntity {
  const _$PetsListEntityImpl(
      {final List<PetEntity>? data, this.currentPage, this.lastPage})
      : _data = data;

  final List<PetEntity>? _data;
  @override
  List<PetEntity>? get data {
    final value = _data;
    if (value == null) return null;
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final int? currentPage;
  @override
  final int? lastPage;

  @override
  String toString() {
    return 'PetsListEntity(data: $data, currentPage: $currentPage, lastPage: $lastPage)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PetsListEntityImpl &&
            const DeepCollectionEquality().equals(other._data, _data) &&
            (identical(other.currentPage, currentPage) ||
                other.currentPage == currentPage) &&
            (identical(other.lastPage, lastPage) ||
                other.lastPage == lastPage));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_data), currentPage, lastPage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PetsListEntityImplCopyWith<_$PetsListEntityImpl> get copyWith =>
      __$$PetsListEntityImplCopyWithImpl<_$PetsListEntityImpl>(
          this, _$identity);
}

abstract class _PetsListEntity implements PetsListEntity {
  const factory _PetsListEntity(
      {final List<PetEntity>? data,
      final int? currentPage,
      final int? lastPage}) = _$PetsListEntityImpl;

  @override
  List<PetEntity>? get data;
  @override
  int? get currentPage;
  @override
  int? get lastPage;
  @override
  @JsonKey(ignore: true)
  _$$PetsListEntityImplCopyWith<_$PetsListEntityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

PetEntity _$PetEntityFromJson(Map<String, dynamic> json) {
  return _PetEntity.fromJson(json);
}

/// @nodoc
mixin _$PetEntity {
  String? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  BreedEntity? get breed => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PetEntityCopyWith<PetEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PetEntityCopyWith<$Res> {
  factory $PetEntityCopyWith(PetEntity value, $Res Function(PetEntity) then) =
      _$PetEntityCopyWithImpl<$Res, PetEntity>;
  @useResult
  $Res call(
      {String? id, String? name, String? description, BreedEntity? breed});

  $BreedEntityCopyWith<$Res>? get breed;
}

/// @nodoc
class _$PetEntityCopyWithImpl<$Res, $Val extends PetEntity>
    implements $PetEntityCopyWith<$Res> {
  _$PetEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? breed = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      breed: freezed == breed
          ? _value.breed
          : breed // ignore: cast_nullable_to_non_nullable
              as BreedEntity?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $BreedEntityCopyWith<$Res>? get breed {
    if (_value.breed == null) {
      return null;
    }

    return $BreedEntityCopyWith<$Res>(_value.breed!, (value) {
      return _then(_value.copyWith(breed: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$PetEntityImplCopyWith<$Res>
    implements $PetEntityCopyWith<$Res> {
  factory _$$PetEntityImplCopyWith(
          _$PetEntityImpl value, $Res Function(_$PetEntityImpl) then) =
      __$$PetEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? id, String? name, String? description, BreedEntity? breed});

  @override
  $BreedEntityCopyWith<$Res>? get breed;
}

/// @nodoc
class __$$PetEntityImplCopyWithImpl<$Res>
    extends _$PetEntityCopyWithImpl<$Res, _$PetEntityImpl>
    implements _$$PetEntityImplCopyWith<$Res> {
  __$$PetEntityImplCopyWithImpl(
      _$PetEntityImpl _value, $Res Function(_$PetEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? breed = freezed,
  }) {
    return _then(_$PetEntityImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      breed: freezed == breed
          ? _value.breed
          : breed // ignore: cast_nullable_to_non_nullable
              as BreedEntity?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$PetEntityImpl extends _PetEntity {
  const _$PetEntityImpl({this.id, this.name, this.description, this.breed})
      : super._();

  factory _$PetEntityImpl.fromJson(Map<String, dynamic> json) =>
      _$$PetEntityImplFromJson(json);

  @override
  final String? id;
  @override
  final String? name;
  @override
  final String? description;
  @override
  final BreedEntity? breed;

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PetEntityImplCopyWith<_$PetEntityImpl> get copyWith =>
      __$$PetEntityImplCopyWithImpl<_$PetEntityImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$PetEntityImplToJson(
      this,
    );
  }
}

abstract class _PetEntity extends PetEntity {
  const factory _PetEntity(
      {final String? id,
      final String? name,
      final String? description,
      final BreedEntity? breed}) = _$PetEntityImpl;
  const _PetEntity._() : super._();

  factory _PetEntity.fromJson(Map<String, dynamic> json) =
      _$PetEntityImpl.fromJson;

  @override
  String? get id;
  @override
  String? get name;
  @override
  String? get description;
  @override
  BreedEntity? get breed;
  @override
  @JsonKey(ignore: true)
  _$$PetEntityImplCopyWith<_$PetEntityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
