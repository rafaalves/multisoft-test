import 'package:multisoft_test/features/breeds/domain/entities/breed/breed.dart';

class SaveParams {
  final String? id;
  final String name;
  final String description;
  final BreedEntity breed;

  const SaveParams({
    this.id,
    required this.name,
    required this.description,
    required this.breed,
  });
}
