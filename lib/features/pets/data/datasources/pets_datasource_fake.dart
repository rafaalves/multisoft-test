import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../../domain/entities/entities.dart';
import '../models/models.dart';
import 'datasources.dart';

PetsListResponse fakeData = PetsListResponse.fromJson({
  "data": List.generate(20, (index) {
    return {
      "id": "${index + 1}",
      "name": "PET ${(index + 1).toString().padLeft(3, '0')}",
      "description":
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "breed": {
        "id": "1",
        "name": "Cat",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "imageUrl": "https://cdn2.thecatapi.com/images/ozEvzdVM-.jpg"
      }
    };
  }).toList()
});

class PetsRemoteDatasourceFake implements IPetsRemoteDatasource {
  @override
  Future<Either<IFailure, bool>> save(SaveParams params) async {
    await Future.delayed(const Duration(seconds: 2));
    return const Right(true);
  }

  @override
  Future<Either<IFailure, bool>> delete(DeleteParams params) async {
    await Future.delayed(const Duration(seconds: 2));
    return const Right(true);
  }

  @override
  Future<Either<IFailure, PetsListResponse>> list(ListParams params) async {
    await Future.delayed(const Duration(seconds: 2));
    return Right(fakeData);
  }
}
