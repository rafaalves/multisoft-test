import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../../domain/entities/entities.dart';
import '../models/models.dart';

abstract class IPetsRemoteDatasource {
  Future<Either<IFailure, PetsListResponse>> list(ListParams params);
  Future<Either<IFailure, bool>> save(SaveParams params);
  Future<Either<IFailure, bool>> delete(DeleteParams params);
}
