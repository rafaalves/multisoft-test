import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../domain/domain.dart';
import '../models/models.dart';
import 'pets_remote_datasource.dart';

class PetsRemoteDatasourceImp extends IPetsRemoteDatasource with MainBoxMixin {
  @override
  Future<Either<IFailure, PetsListResponse>> list(ListParams params) async {
    try {
      final catMap = await MainBoxMixin.petBox.getAllValues();
      final catList = jsonDecode(jsonEncode(catMap.values.toList()));
      await Future.delayed(const Duration(milliseconds: 100));
      return Right(PetsListResponse.fromJson({"data": catList}));
    } catch (err) {
      log.e(err.toString(), error: err);
    }

    await Future.delayed(const Duration(milliseconds: 100));
    return const Left(ServerFailure());
  }

  @override
  Future<Either<IFailure, bool>> delete(DeleteParams params) async {
    try {
      await MainBoxMixin.petBox.delete(params.id);
      await Future.delayed(const Duration(milliseconds: 100));
      return const Right(true);
    } catch (err) {
      log.e(err.toString(), error: err);
      await Future.delayed(const Duration(milliseconds: 100));
      return const Left(ServerFailure());
    }
  }

  @override
  Future<Either<IFailure, bool>> save(SaveParams params) async {
    try {
      final id = params.id ?? DateTime.now().millisecondsSinceEpoch.toString();
      await MainBoxMixin.petBox.put(
        id,
        PetEntity(
          id: id,
          name: params.name,
          description: params.description,
          breed: params.breed,
        ).toJson(),
      );

      await Future.delayed(const Duration(milliseconds: 100));
      return const Right(true);
    } catch (err) {
      log.e(err.toString(), error: err);
      await Future.delayed(const Duration(milliseconds: 100));
      return const Left(ServerFailure());
    }
  }
}
