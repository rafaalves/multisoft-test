// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'pets_list_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PetsListResponse _$PetsListResponseFromJson(Map<String, dynamic> json) {
  return _PetsListResponse.fromJson(json);
}

/// @nodoc
mixin _$PetsListResponse {
  List<DataPet>? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PetsListResponseCopyWith<PetsListResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PetsListResponseCopyWith<$Res> {
  factory $PetsListResponseCopyWith(
          PetsListResponse value, $Res Function(PetsListResponse) then) =
      _$PetsListResponseCopyWithImpl<$Res, PetsListResponse>;
  @useResult
  $Res call({List<DataPet>? data});
}

/// @nodoc
class _$PetsListResponseCopyWithImpl<$Res, $Val extends PetsListResponse>
    implements $PetsListResponseCopyWith<$Res> {
  _$PetsListResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<DataPet>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PetsListResponseImplCopyWith<$Res>
    implements $PetsListResponseCopyWith<$Res> {
  factory _$$PetsListResponseImplCopyWith(_$PetsListResponseImpl value,
          $Res Function(_$PetsListResponseImpl) then) =
      __$$PetsListResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<DataPet>? data});
}

/// @nodoc
class __$$PetsListResponseImplCopyWithImpl<$Res>
    extends _$PetsListResponseCopyWithImpl<$Res, _$PetsListResponseImpl>
    implements _$$PetsListResponseImplCopyWith<$Res> {
  __$$PetsListResponseImplCopyWithImpl(_$PetsListResponseImpl _value,
      $Res Function(_$PetsListResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$PetsListResponseImpl(
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<DataPet>?,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$PetsListResponseImpl extends _PetsListResponse {
  const _$PetsListResponseImpl({final List<DataPet>? data})
      : _data = data,
        super._();

  factory _$PetsListResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$PetsListResponseImplFromJson(json);

  final List<DataPet>? _data;
  @override
  List<DataPet>? get data {
    final value = _data;
    if (value == null) return null;
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'PetsListResponse(data: $data)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PetsListResponseImpl &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PetsListResponseImplCopyWith<_$PetsListResponseImpl> get copyWith =>
      __$$PetsListResponseImplCopyWithImpl<_$PetsListResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$PetsListResponseImplToJson(
      this,
    );
  }
}

abstract class _PetsListResponse extends PetsListResponse {
  const factory _PetsListResponse({final List<DataPet>? data}) =
      _$PetsListResponseImpl;
  const _PetsListResponse._() : super._();

  factory _PetsListResponse.fromJson(Map<String, dynamic> json) =
      _$PetsListResponseImpl.fromJson;

  @override
  List<DataPet>? get data;
  @override
  @JsonKey(ignore: true)
  _$$PetsListResponseImplCopyWith<_$PetsListResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

DataPet _$DataPetFromJson(Map<String, dynamic> json) {
  return _DataPet.fromJson(json);
}

/// @nodoc
mixin _$DataPet {
  dynamic get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  BreedEntity? get breed => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DataPetCopyWith<DataPet> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataPetCopyWith<$Res> {
  factory $DataPetCopyWith(DataPet value, $Res Function(DataPet) then) =
      _$DataPetCopyWithImpl<$Res, DataPet>;
  @useResult
  $Res call({dynamic id, String name, String? description, BreedEntity? breed});

  $BreedEntityCopyWith<$Res>? get breed;
}

/// @nodoc
class _$DataPetCopyWithImpl<$Res, $Val extends DataPet>
    implements $DataPetCopyWith<$Res> {
  _$DataPetCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = null,
    Object? description = freezed,
    Object? breed = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      breed: freezed == breed
          ? _value.breed
          : breed // ignore: cast_nullable_to_non_nullable
              as BreedEntity?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $BreedEntityCopyWith<$Res>? get breed {
    if (_value.breed == null) {
      return null;
    }

    return $BreedEntityCopyWith<$Res>(_value.breed!, (value) {
      return _then(_value.copyWith(breed: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$DataPetImplCopyWith<$Res> implements $DataPetCopyWith<$Res> {
  factory _$$DataPetImplCopyWith(
          _$DataPetImpl value, $Res Function(_$DataPetImpl) then) =
      __$$DataPetImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({dynamic id, String name, String? description, BreedEntity? breed});

  @override
  $BreedEntityCopyWith<$Res>? get breed;
}

/// @nodoc
class __$$DataPetImplCopyWithImpl<$Res>
    extends _$DataPetCopyWithImpl<$Res, _$DataPetImpl>
    implements _$$DataPetImplCopyWith<$Res> {
  __$$DataPetImplCopyWithImpl(
      _$DataPetImpl _value, $Res Function(_$DataPetImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = null,
    Object? description = freezed,
    Object? breed = freezed,
  }) {
    return _then(_$DataPetImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      breed: freezed == breed
          ? _value.breed
          : breed // ignore: cast_nullable_to_non_nullable
              as BreedEntity?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$DataPetImpl implements _DataPet {
  const _$DataPetImpl(
      {required this.id, required this.name, this.description, this.breed});

  factory _$DataPetImpl.fromJson(Map<String, dynamic> json) =>
      _$$DataPetImplFromJson(json);

  @override
  final dynamic id;
  @override
  final String name;
  @override
  final String? description;
  @override
  final BreedEntity? breed;

  @override
  String toString() {
    return 'DataPet(id: $id, name: $name, description: $description, breed: $breed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataPetImpl &&
            const DeepCollectionEquality().equals(other.id, id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.breed, breed) || other.breed == breed));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(id), name, description, breed);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataPetImplCopyWith<_$DataPetImpl> get copyWith =>
      __$$DataPetImplCopyWithImpl<_$DataPetImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$DataPetImplToJson(
      this,
    );
  }
}

abstract class _DataPet implements DataPet {
  const factory _DataPet(
      {required final dynamic id,
      required final String name,
      final String? description,
      final BreedEntity? breed}) = _$DataPetImpl;

  factory _DataPet.fromJson(Map<String, dynamic> json) = _$DataPetImpl.fromJson;

  @override
  dynamic get id;
  @override
  String get name;
  @override
  String? get description;
  @override
  BreedEntity? get breed;
  @override
  @JsonKey(ignore: true)
  _$$DataPetImplCopyWith<_$DataPetImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
