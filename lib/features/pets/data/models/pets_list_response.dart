import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/features/breeds/domain/entities/breed/breed.dart';

import '../../domain/entities/entities.dart';

part 'pets_list_response.freezed.dart';
part 'pets_list_response.g.dart';

@freezed
class PetsListResponse with _$PetsListResponse {
  const PetsListResponse._();

  // ignore: invalid_annotation_target
  @JsonSerializable(explicitToJson: true)
  const factory PetsListResponse({
    List<DataPet>? data,
  }) = _PetsListResponse;

  factory PetsListResponse.fromJson(Map<String, dynamic> json) => _$PetsListResponseFromJson(json);

  PetsListEntity toEntity() {
    final list = data!.map<PetEntity>((model) {
      return PetEntity(
        id: model.id.toString(),
        name: model.name,
        description: model.description,
        breed: model.breed,
      );
    }).toList();

    return PetsListEntity(
      data: list,
    );
  }
}

@freezed
class DataPet with _$DataPet {
  const factory DataPet({
    required dynamic id,
    required String name,
    String? description,
    BreedEntity? breed,
  }) = _DataPet;

  factory DataPet.fromJson(Map<String, dynamic> json) => _$DataPetFromJson(json);
}
