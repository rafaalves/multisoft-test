// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pets_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PetsListResponseImpl _$$PetsListResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$PetsListResponseImpl(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => DataPet.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$PetsListResponseImplToJson(
        _$PetsListResponseImpl instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e.toJson()).toList(),
    };

_$DataPetImpl _$$DataPetImplFromJson(Map<String, dynamic> json) =>
    _$DataPetImpl(
      id: json['id'],
      name: json['name'] as String,
      description: json['description'] as String?,
      breed: json['breed'] == null
          ? null
          : BreedEntity.fromJson(json['breed'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$DataPetImplToJson(_$DataPetImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'breed': instance.breed?.toJson(),
    };
