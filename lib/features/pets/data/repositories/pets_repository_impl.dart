import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../../domain/entities/entities.dart';
import '../../domain/repositories/repositories.dart';
import '../datasources/datasources.dart';

class PetsRepositoryImpl implements IPetsRepository {
  final IPetsRemoteDatasource _datasource;

  const PetsRepositoryImpl(this._datasource);

  @override
  Future<Either<IFailure, PetsListEntity>> getList(ListParams params) async {
    final response = await _datasource.list(params);

    return response.fold(
          (failure) => Left(failure),
          (response) {
            if (response.data?.isEmpty ?? true) {
              return Left(NoDataFailure());
            }
            return Right(response.toEntity());
          },
    );
  }

  @override
  Future<Either<IFailure, bool>> save(SaveParams params) async {
      final response = await _datasource.save(params);

      return response.fold(
        (failure) => Left(failure),
        (response) => Right(response),
      );
  }

  @override
  Future<Either<IFailure, bool>> delete(DeleteParams params) async {
    final response = await _datasource.delete(params);

    return response.fold(
          (failure) => Left(failure),
          (response) => Right(response),
    );
  }
}
