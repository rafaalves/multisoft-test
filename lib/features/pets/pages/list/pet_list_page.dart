import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multisoft_test/core/core.dart';

import '../../domain/domain.dart';
import 'cubit/list_pet_cubit.dart';

class PetListPage extends StatefulWidget {
  const PetListPage({super.key});

  @override
  State<PetListPage> createState() => _PetListPageState();
}

class _PetListPageState extends State<PetListPage> {
  @override
  void initState() {
    super.initState();
    context.read<ListPetCubit>().fetchUsers();
  }

  @override
  Widget build(BuildContext context) {
    return AppPrimaryPage(
      appBar: AppTextBar(title: Strings.of(context)!.petListPage),
      bottomNavigation: _buildAddButton(),
      child: BlocBuilder<ListPetCubit, ListPetState>(
        builder: (context, state) {
          return state.when(
            loading: () => const Center(child: AppLoading()),
            success: (items) {
              return _buildList(items);
            },
            failure: (message) => Center(child: AppEmpty(errorMessage: message)),
            empty: () => const Center(child: AppEmpty()),
          );
        },
      ),
    );
  }

  Widget _buildAddButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AppRaisedButton(
        labelText: Strings.of(context)!.add,
        onPressed: () {
          context.push(AppRoutes.petEdit.path);
        },
      ),
    );
  }

  Widget _buildList(List<PetEntity> pets) {
    return ListView.builder(
      itemCount: pets.length,
      padding: EdgeInsets.symmetric(vertical: Dimens.space16),
      itemBuilder: (_, index) {
        if (index >= pets.length) {
          return Padding(
            padding: EdgeInsets.all(Dimens.space16),
            child: const Center(
              child: CircularProgressIndicator.adaptive(),
            ),
          );
        } else {
          final pet = pets[index];
          return InkWell(
            child: _buildItem(pet),
            onTap: () {},
          );
        }
      },
    );
  }

  Widget _buildItem(PetEntity pet) {
    return AppCard(
      heroTag: 'pet-${pet.id}',
      margin: EdgeInsets.all(Dimens.space8),
      imageUrl: pet.breed?.imageUrl,
      titleText: pet.name,
      bodyText: pet.description,
      onPressed: () {
        context.push(
          AppRoutes.petView.path,
          extra: pet.toJson(),
        );
      },
    );
  }
}
