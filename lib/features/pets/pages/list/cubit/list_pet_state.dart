part of 'list_pet_cubit.dart';

@freezed
class ListPetState with _$ListPetState {
  const factory ListPetState.loading() = _Loading;
  const factory ListPetState.success(List<PetEntity> data) = _Success;
  const factory ListPetState.failure(String message) = _Failure;
  const factory ListPetState.empty() = _Empty;
}
