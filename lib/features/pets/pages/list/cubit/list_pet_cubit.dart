import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/core/core.dart';

import '../../../domain/domain.dart';

part 'list_pet_cubit.freezed.dart';
part 'list_pet_state.dart';

class ListPetCubit extends Cubit<ListPetState> {
  final GetPetsUsecase _usecase;

  ListPetCubit(this._usecase) : super(const _Loading());

  Future<void> fetchUsers() async {
    await _fetchData(const ListParams());
  }

  Future<void> _fetchData(ListParams params) async {
    emit(const _Loading());

    final data = await _usecase.call(params);
    data.fold(
      (l) {
        if (l is ServerFailure) {
          emit(_Failure(l.message ?? ""));
        } else if (l is NoDataFailure) {
          emit(const _Empty());
        }
      },
      (r) {
        final items = r.data ?? [];
        emit(_Success(items));
      },
    );
  }
}
