import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/core/core.dart';

import '../../../domain/domain.dart';

part 'delete_pet_cubit.freezed.dart';
part 'delete_pet_state.dart';

class DeletePetCubit extends Cubit<DeletePetState> {
  final DeletePetUsecase _usecase;

  DeletePetCubit(this._usecase) : super(const _Init());

  Future<void> delete(String id) async {
    emit(const _Loading());
    final data = await _usecase(DeleteParams(id: id));

    data.fold(
      (l) {
        if (l is ServerFailure) {
          emit(_Failure(
            l.title,
            l.message ?? '',
          ));
        }
      },
      (r) {
        emit(const _Success());
      },
    );
  }
}
