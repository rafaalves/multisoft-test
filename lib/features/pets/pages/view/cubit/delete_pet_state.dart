part of 'delete_pet_cubit.dart';

@freezed
class DeletePetState with _$DeletePetState {
  const factory DeletePetState.init() = _Init;
  const factory DeletePetState.loading() = _Loading;
  const factory DeletePetState.success() = _Success;
  const factory DeletePetState.failure(String? title, String message) = _Failure;
}
