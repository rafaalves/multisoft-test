import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../domain/domain.dart';
import '../list/cubit/list_pet_cubit.dart';
import 'cubit/delete_pet_cubit.dart';

class PetViewPage extends StatefulWidget {
  final PetEntity pet;

  const PetViewPage({
    super.key,
    required this.pet,
  });

  @override
  State<PetViewPage> createState() => _PetViewPageState();
}

class _PetViewPageState extends State<PetViewPage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<DeletePetCubit, DeletePetState>(
      listener: (context, state) {
        state.whenOrNull(
          loading: () {
            context.show();
          },
          success: () {
            context.dismiss();
            context.pop();
            context.read<ListPetCubit>().fetchUsers();
          },
          failure: (title, message) {
            context.dismiss();
          },
        );
      },
      child: AppPrimaryPage(
        appBar: AppTextBar(
          title: Strings.of(context)!.petViewPage,
        ),
        bottomNavigation: _buildButton(),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: Dimens.space32,
              horizontal: Dimens.space24,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Hero(
                    tag: 'pet-${widget.pet.id}-image',
                    child: AppImage(
                      url: widget.pet.breed?.imageUrl ?? '',
                      size: Dimens.profilePictureSize,
                    ),
                  ),
                ),
                AppSpacerVertical(value: Dimens.space24),
                Hero(
                  tag: 'pet-${widget.pet.id}-title',
                  child: Text(
                    widget.pet.name ?? '',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                Text(
                  widget.pet.breed?.name ?? '',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                AppSpacerVertical(value: Dimens.space24),
                Hero(
                  tag: 'pet-${widget.pet.id}-body',
                  child: Text(
                    widget.pet.description ?? '',
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: AppRaisedButton(
            labelText: Strings.of(context)!.delete,
            color: Theme.of(context).extension<AppColors>()!.red,
            onPressed: () {
              context.openDangerAlert(
                title: Strings.of(context)!.attention,
                description: Strings.of(context)!.petAlertDeletePet,
                positiveLabelButton: Strings.of(context)!.delete,
                onPressed: () {
                  context.read<DeletePetCubit>().delete(widget.pet.id!);
                },
              );
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: AppRaisedButton(
            labelText: Strings.of(context)!.edit,
            onPressed: () {
              context.push(
                AppRoutes.petEdit.path,
                extra: widget.pet.toJson(),
              );
            },
          ),
        ),
      ],
    );
  }
}
