import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../../breeds/domain/domain.dart';
import '../../domain/domain.dart';
import '../pages.dart';

class PetEditPage extends StatefulWidget {
  final PetEntity? pet;

  const PetEditPage({
    super.key,
    this.pet,
  });

  @override
  State<PetEditPage> createState() => _PetEditPageState();
}

class _PetEditPageState extends State<PetEditPage> {
  late TextEditingController _nameController;
  late TextEditingController _descriptionController;
  final _formKey = GlobalKey<FormState>();
  late PetEntity _pet;
  BreedEntity? _breed;

  @override
  void initState() {
    super.initState();
    _pet = widget.pet?.copyWith() ?? const PetEntity();
    _breed = _pet.breed;
    _nameController = TextEditingController(text: _pet.name);
    _descriptionController = TextEditingController(text: _pet.description);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EditPetCubit, EditPetState>(
      listener: (_, state) {
        state.whenOrNull(
          loading: () {
            context.show();
          },
          success: () {
            context.dismiss();
            context.pop();
            context.read<ListPetCubit>().fetchUsers();
          },
          failure: (title, message) {
            context.dismiss();
          },
        );
      },
      child: AppPrimaryPage(
        appBar: AppTextBar(
          title: Strings.of(context)!.petEditPage,
          subtitle: widget.pet?.id != null ? widget.pet?.name : Strings.of(context)!.petAddPage,
        ),
        bottomNavigation: _buildButtonSave(),
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      AppTextField(
                        labelText: Strings.of(context)!.petTextFieldName,
                        placeholder: Strings.of(context)!.petTextFieldNameHint,
                        controller: _nameController,
                      ),
                      AppTextField(
                        labelText: Strings.of(context)!.petTextFieldDescription,
                        placeholder: Strings.of(context)!.petTextFieldDescriptionHint,
                        controller: _descriptionController,
                        maxLines: 3,
                        textInputAction: TextInputAction.newline,
                      ),
                      if (_breed != null)
                        AppCard(
                          heroTag: _breed?.id != null ? 'breed-${_breed!.id}' : null,
                          imageUrl: _breed?.imageUrl,
                          titleText: _breed?.name ?? '',
                          bodyText: _breed?.description ?? '',
                        ),
                      AppTextButton(
                        labelText: Strings.of(context)!.petButtonBreedLabel,
                        onPressed: () async {
                          FocusScope.of(context).requestFocus(FocusNode());
                          final breed = await context.push(AppRoutes.breedList.path) as BreedEntity;
                          _breed = breed;
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButtonSave() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AppRaisedButton(
        width: double.infinity,
        labelText: Strings.of(context)!.buttonSave,
        onPressed: _submit,
      ),
    );
  }

  void _submit() {
    FocusScope.of(context).requestFocus(FocusNode());
    final form = _formKey.currentState!;

    if (form.validate()) {
      if (_breed == null) {
        Strings.of(context)!.petAlertBreedLabel.toToastError(context);
        return;
      }

      context.read<EditPetCubit>().save(
            id: widget.pet?.id,
            name: _nameController.text,
            description: _descriptionController.text,
            breed: _breed!,
          );
    }
  }
}
