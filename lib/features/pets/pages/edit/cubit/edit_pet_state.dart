part of 'edit_pet_cubit.dart';

@freezed
class EditPetState with _$EditPetState {
  const factory EditPetState.init() = _Init;
  const factory EditPetState.loading() = _Loading;
  const factory EditPetState.success() = _Success;
  const factory EditPetState.failure(String? title, String message) = _Failure;
}
