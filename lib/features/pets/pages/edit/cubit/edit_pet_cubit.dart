import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/core/core.dart';

import '../../../../breeds/domain/domain.dart';
import '../../../domain/domain.dart';

part 'edit_pet_cubit.freezed.dart';
part 'edit_pet_state.dart';

class EditPetCubit extends Cubit<EditPetState> {
  final SavePetUsecase _usecase;

  EditPetCubit(this._usecase) : super(const _Init());

  bool loading = false;

  Future<void> save({
    String? id,
    required String name,
    required String description,
    required BreedEntity breed,
  }) async {
    emit(const _Loading());
    loading = true;

    final data = await _usecase(SaveParams(
      id: id,
      name: name,
      description: description,
      breed: breed,
    ));

    data.fold(
      (l) {
        loading = false;
        if (l is ServerFailure) {
          emit(_Failure(
            l.title,
            l.message ?? '',
          ));
        }
      },
      (r) {
        loading = false;
        emit(const _Success());
      },
    );
  }
}
