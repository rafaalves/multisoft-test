// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'breeds_list_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

BreedsListResponse _$BreedsListResponseFromJson(Map<String, dynamic> json) {
  return _BreedsListResponse.fromJson(json);
}

/// @nodoc
mixin _$BreedsListResponse {
  List<DataBreed>? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BreedsListResponseCopyWith<BreedsListResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BreedsListResponseCopyWith<$Res> {
  factory $BreedsListResponseCopyWith(
          BreedsListResponse value, $Res Function(BreedsListResponse) then) =
      _$BreedsListResponseCopyWithImpl<$Res, BreedsListResponse>;
  @useResult
  $Res call({List<DataBreed>? data});
}

/// @nodoc
class _$BreedsListResponseCopyWithImpl<$Res, $Val extends BreedsListResponse>
    implements $BreedsListResponseCopyWith<$Res> {
  _$BreedsListResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<DataBreed>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BreedsListResponseImplCopyWith<$Res>
    implements $BreedsListResponseCopyWith<$Res> {
  factory _$$BreedsListResponseImplCopyWith(_$BreedsListResponseImpl value,
          $Res Function(_$BreedsListResponseImpl) then) =
      __$$BreedsListResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<DataBreed>? data});
}

/// @nodoc
class __$$BreedsListResponseImplCopyWithImpl<$Res>
    extends _$BreedsListResponseCopyWithImpl<$Res, _$BreedsListResponseImpl>
    implements _$$BreedsListResponseImplCopyWith<$Res> {
  __$$BreedsListResponseImplCopyWithImpl(_$BreedsListResponseImpl _value,
      $Res Function(_$BreedsListResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$BreedsListResponseImpl(
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<DataBreed>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$BreedsListResponseImpl extends _BreedsListResponse {
  const _$BreedsListResponseImpl({final List<DataBreed>? data})
      : _data = data,
        super._();

  factory _$BreedsListResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$BreedsListResponseImplFromJson(json);

  final List<DataBreed>? _data;
  @override
  List<DataBreed>? get data {
    final value = _data;
    if (value == null) return null;
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'BreedsListResponse(data: $data)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BreedsListResponseImpl &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BreedsListResponseImplCopyWith<_$BreedsListResponseImpl> get copyWith =>
      __$$BreedsListResponseImplCopyWithImpl<_$BreedsListResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$BreedsListResponseImplToJson(
      this,
    );
  }
}

abstract class _BreedsListResponse extends BreedsListResponse {
  const factory _BreedsListResponse({final List<DataBreed>? data}) =
      _$BreedsListResponseImpl;
  const _BreedsListResponse._() : super._();

  factory _BreedsListResponse.fromJson(Map<String, dynamic> json) =
      _$BreedsListResponseImpl.fromJson;

  @override
  List<DataBreed>? get data;
  @override
  @JsonKey(ignore: true)
  _$$BreedsListResponseImplCopyWith<_$BreedsListResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

DataBreed _$DataBreedFromJson(Map<String, dynamic> json) {
  return _DataBreed.fromJson(json);
}

/// @nodoc
mixin _$DataBreed {
  dynamic get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'reference_image_id')
  String? get imageId => throw _privateConstructorUsedError;
  ImageBreed? get image => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DataBreedCopyWith<DataBreed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataBreedCopyWith<$Res> {
  factory $DataBreedCopyWith(DataBreed value, $Res Function(DataBreed) then) =
      _$DataBreedCopyWithImpl<$Res, DataBreed>;
  @useResult
  $Res call(
      {dynamic id,
      String name,
      String? description,
      @JsonKey(name: 'reference_image_id') String? imageId,
      ImageBreed? image});

  $ImageBreedCopyWith<$Res>? get image;
}

/// @nodoc
class _$DataBreedCopyWithImpl<$Res, $Val extends DataBreed>
    implements $DataBreedCopyWith<$Res> {
  _$DataBreedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = null,
    Object? description = freezed,
    Object? imageId = freezed,
    Object? image = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      imageId: freezed == imageId
          ? _value.imageId
          : imageId // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as ImageBreed?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ImageBreedCopyWith<$Res>? get image {
    if (_value.image == null) {
      return null;
    }

    return $ImageBreedCopyWith<$Res>(_value.image!, (value) {
      return _then(_value.copyWith(image: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$DataBreedImplCopyWith<$Res>
    implements $DataBreedCopyWith<$Res> {
  factory _$$DataBreedImplCopyWith(
          _$DataBreedImpl value, $Res Function(_$DataBreedImpl) then) =
      __$$DataBreedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {dynamic id,
      String name,
      String? description,
      @JsonKey(name: 'reference_image_id') String? imageId,
      ImageBreed? image});

  @override
  $ImageBreedCopyWith<$Res>? get image;
}

/// @nodoc
class __$$DataBreedImplCopyWithImpl<$Res>
    extends _$DataBreedCopyWithImpl<$Res, _$DataBreedImpl>
    implements _$$DataBreedImplCopyWith<$Res> {
  __$$DataBreedImplCopyWithImpl(
      _$DataBreedImpl _value, $Res Function(_$DataBreedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = null,
    Object? description = freezed,
    Object? imageId = freezed,
    Object? image = freezed,
  }) {
    return _then(_$DataBreedImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      imageId: freezed == imageId
          ? _value.imageId
          : imageId // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as ImageBreed?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$DataBreedImpl implements _DataBreed {
  const _$DataBreedImpl(
      {required this.id,
      required this.name,
      this.description,
      @JsonKey(name: 'reference_image_id') this.imageId,
      this.image});

  factory _$DataBreedImpl.fromJson(Map<String, dynamic> json) =>
      _$$DataBreedImplFromJson(json);

  @override
  final dynamic id;
  @override
  final String name;
  @override
  final String? description;
  @override
  @JsonKey(name: 'reference_image_id')
  final String? imageId;
  @override
  final ImageBreed? image;

  @override
  String toString() {
    return 'DataBreed(id: $id, name: $name, description: $description, imageId: $imageId, image: $image)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataBreedImpl &&
            const DeepCollectionEquality().equals(other.id, id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.imageId, imageId) || other.imageId == imageId) &&
            (identical(other.image, image) || other.image == image));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      name,
      description,
      imageId,
      image);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataBreedImplCopyWith<_$DataBreedImpl> get copyWith =>
      __$$DataBreedImplCopyWithImpl<_$DataBreedImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$DataBreedImplToJson(
      this,
    );
  }
}

abstract class _DataBreed implements DataBreed {
  const factory _DataBreed(
      {required final dynamic id,
      required final String name,
      final String? description,
      @JsonKey(name: 'reference_image_id') final String? imageId,
      final ImageBreed? image}) = _$DataBreedImpl;

  factory _DataBreed.fromJson(Map<String, dynamic> json) =
      _$DataBreedImpl.fromJson;

  @override
  dynamic get id;
  @override
  String get name;
  @override
  String? get description;
  @override
  @JsonKey(name: 'reference_image_id')
  String? get imageId;
  @override
  ImageBreed? get image;
  @override
  @JsonKey(ignore: true)
  _$$DataBreedImplCopyWith<_$DataBreedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

ImageBreed _$ImageBreedFromJson(Map<String, dynamic> json) {
  return _ImageBreed.fromJson(json);
}

/// @nodoc
mixin _$ImageBreed {
  String get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ImageBreedCopyWith<ImageBreed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImageBreedCopyWith<$Res> {
  factory $ImageBreedCopyWith(
          ImageBreed value, $Res Function(ImageBreed) then) =
      _$ImageBreedCopyWithImpl<$Res, ImageBreed>;
  @useResult
  $Res call({String url});
}

/// @nodoc
class _$ImageBreedCopyWithImpl<$Res, $Val extends ImageBreed>
    implements $ImageBreedCopyWith<$Res> {
  _$ImageBreedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? url = null,
  }) {
    return _then(_value.copyWith(
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ImageBreedImplCopyWith<$Res>
    implements $ImageBreedCopyWith<$Res> {
  factory _$$ImageBreedImplCopyWith(
          _$ImageBreedImpl value, $Res Function(_$ImageBreedImpl) then) =
      __$$ImageBreedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String url});
}

/// @nodoc
class __$$ImageBreedImplCopyWithImpl<$Res>
    extends _$ImageBreedCopyWithImpl<$Res, _$ImageBreedImpl>
    implements _$$ImageBreedImplCopyWith<$Res> {
  __$$ImageBreedImplCopyWithImpl(
      _$ImageBreedImpl _value, $Res Function(_$ImageBreedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? url = null,
  }) {
    return _then(_$ImageBreedImpl(
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ImageBreedImpl implements _ImageBreed {
  const _$ImageBreedImpl({required this.url});

  factory _$ImageBreedImpl.fromJson(Map<String, dynamic> json) =>
      _$$ImageBreedImplFromJson(json);

  @override
  final String url;

  @override
  String toString() {
    return 'ImageBreed(url: $url)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ImageBreedImpl &&
            (identical(other.url, url) || other.url == url));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, url);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ImageBreedImplCopyWith<_$ImageBreedImpl> get copyWith =>
      __$$ImageBreedImplCopyWithImpl<_$ImageBreedImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ImageBreedImplToJson(
      this,
    );
  }
}

abstract class _ImageBreed implements ImageBreed {
  const factory _ImageBreed({required final String url}) = _$ImageBreedImpl;

  factory _ImageBreed.fromJson(Map<String, dynamic> json) =
      _$ImageBreedImpl.fromJson;

  @override
  String get url;
  @override
  @JsonKey(ignore: true)
  _$$ImageBreedImplCopyWith<_$ImageBreedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
