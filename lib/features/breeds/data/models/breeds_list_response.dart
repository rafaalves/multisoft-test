import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/core/api/api.dart';

import '../../domain/domain.dart';

part 'breeds_list_response.freezed.dart';
part 'breeds_list_response.g.dart';

@freezed
class BreedsListResponse with _$BreedsListResponse {
  const BreedsListResponse._();

  const factory BreedsListResponse({
    List<DataBreed>? data,
  }) = _BreedsListResponse;

  factory BreedsListResponse.fromJson(Map<String, dynamic> json) =>
      _$BreedsListResponseFromJson(json);

  BreedsListEntity toEntity() {
    final list = data!.map<BreedEntity>((model) {
      return BreedEntity(
        id: model.id.toString(),
        name: model.name,
        description: model.description,
        imageUrl: model.image?.url ??
            (model.imageId != null
                ? ApiPath.breedBaseImageUrl.replaceAll('{id}', model.imageId!)
                : null),
      );
    }).toList();

    return BreedsListEntity(
      data: list,
    );
  }
}

@freezed
class DataBreed with _$DataBreed {
  const factory DataBreed({
    required dynamic id,
    required String name,
    String? description,
    @JsonKey(name: 'reference_image_id') String? imageId,
    ImageBreed? image,
  }) = _DataBreed;

  factory DataBreed.fromJson(Map<String, dynamic> json) => _$DataBreedFromJson(json);
}

@freezed
class ImageBreed with _$ImageBreed {
  const factory ImageBreed({
    required String url,
  }) = _ImageBreed;

  factory ImageBreed.fromJson(Map<String, dynamic> json) => _$ImageBreedFromJson(json);
}
