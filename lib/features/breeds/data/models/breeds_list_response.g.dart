// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'breeds_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$BreedsListResponseImpl _$$BreedsListResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$BreedsListResponseImpl(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => DataBreed.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$BreedsListResponseImplToJson(
        _$BreedsListResponseImpl instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e.toJson()).toList(),
    };

_$DataBreedImpl _$$DataBreedImplFromJson(Map<String, dynamic> json) =>
    _$DataBreedImpl(
      id: json['id'],
      name: json['name'] as String,
      description: json['description'] as String?,
      imageId: json['reference_image_id'] as String?,
      image: json['image'] == null
          ? null
          : ImageBreed.fromJson(json['image'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$DataBreedImplToJson(_$DataBreedImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'reference_image_id': instance.imageId,
      'image': instance.image?.toJson(),
    };

_$ImageBreedImpl _$$ImageBreedImplFromJson(Map<String, dynamic> json) =>
    _$ImageBreedImpl(
      url: json['url'] as String,
    );

Map<String, dynamic> _$$ImageBreedImplToJson(_$ImageBreedImpl instance) =>
    <String, dynamic>{
      'url': instance.url,
    };
