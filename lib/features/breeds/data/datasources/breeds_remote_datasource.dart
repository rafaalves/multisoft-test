import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../../domain/domain.dart';
import '../models/models.dart';

abstract class IBreedsRemoteDatasource {
  Future<Either<IFailure, BreedsListResponse>> list(BreedsParams params);
}
