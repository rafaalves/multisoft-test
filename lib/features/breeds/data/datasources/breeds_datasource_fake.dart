import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../domain/domain.dart';
import '../models/models.dart';
import 'breeds_remote_datasource.dart';

class BreedsRemoteDatasourceFake implements IBreedsRemoteDatasource {
  @override
  Future<Either<IFailure, BreedsListResponse>> list(BreedsParams params) async {
    try {
      String data = await rootBundle.loadString("assets/stubs/breed_list.json");
      final jsonResult = json.decode(data);
      await Future.delayed(const Duration(seconds: 2));
      return Right(BreedsListResponse.fromJson({
        "data": jsonResult,
        "page": params.page,
      }));
    } catch (err) {
      log.e(err.toString(), error: err);
      return Left(NoDataFailure());
    }
  }
}
