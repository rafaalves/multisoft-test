import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../../domain/domain.dart';
import '../models/models.dart';
import 'datasources.dart';

class BreedsRemoteDatasourceImpl extends IBreedsRemoteDatasource {
  final ApiClient _client;

  BreedsRemoteDatasourceImpl(this._client);

  @override
  Future<Either<IFailure, BreedsListResponse>> list(BreedsParams params) async {
    final response = await _client.getRequest(
      '${ApiPath.breedsList}?limit=10&page=${params.page}',
      baseUrl: ApiPath.breedBaseUrl,
      converter: (response) => BreedsListResponse.fromJson(response),
    );
    return response;
  }
}
