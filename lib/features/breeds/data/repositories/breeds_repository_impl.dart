import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../domain/domain.dart';
import '../datasources/datasources.dart';
import '../models/models.dart';

class BreedsRepositoryImpl with MainBoxMixin implements IBreedsRepository {
  final IBreedsRemoteDatasource _datasource;

  const BreedsRepositoryImpl(this._datasource);

  @override
  Future<Either<IFailure, BreedsListEntity>> getBreeds(BreedsParams params) async {
    final keyCache = 'list_${params.page}';
    final response = await _datasource.list(params);

    return response.fold(
      (failure) async {
        final map = await getCacheBreeds(keyCache);
        if (map != null && map.isNotEmpty) {
          final response = BreedsListResponse.fromJson(map);
          return _getResponse(response);
        }

        return Left(failure);
      },
      (response) async {
        putCacheBreeds(keyCache, response.toJson());
        return _getResponse(response);
      },
    );
  }


  Either<IFailure, BreedsListEntity> _getResponse(BreedsListResponse response) {
    if (response.data?.isEmpty ?? true) {
      return Left(NoDataFailure());
    }
    return Right(response.toEntity());
  }
}
