import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/ext/string.dart';

import '../../domain/domain.dart';
import 'cubit/breeds_cubit.dart';

class BreedListPage extends StatefulWidget {
  const BreedListPage({super.key});

  @override
  State<BreedListPage> createState() => _BreedListPageState();
}

class _BreedListPageState extends State<BreedListPage> {
  final ScrollController _scrollController = ScrollController();
  int _currentPage = 0;
  bool _loading = false;
  bool _final = false;

  final List<BreedEntity> _breeds = [];

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() async {
      if (_scrollController.position.atEdge) {
        if (_scrollController.position.pixels != 0) {
          if (!_final && !_loading) {
            _currentPage++;
            _loading = true;
            await context.read<BreedsCubit>().fetch(BreedsParams(page: _currentPage));
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.of(context)!.breedListTitle),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: RefreshIndicator(
        color: Theme.of(context).extension<AppColors>()!.secondary,
        backgroundColor: Theme.of(context).extension<AppColors>()!.background,
        onRefresh: () {
          _currentPage = 0;
          _loading = true;
          _final = false;
          _breeds.clear();

          return context.read<BreedsCubit>().refresh(BreedsParams(page: _currentPage));
        },
        child: BlocBuilder<BreedsCubit, BreedsState>(
          builder: (context, state) {
            return state.when(
              loading: () => const Center(child: AppLoading()),
              success: (items, message) {
                if (message != null) {
                  message.toToastError(context);
                }
                _breeds.addAll(items);
                _final = items.isEmpty;
                _loading = false;

                return ListView.builder(
                  controller: _scrollController,
                  itemCount: _final ? _breeds.length : _breeds.length + 1,
                  physics: const AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(vertical: Dimens.space16),
                  itemBuilder: (_, index) {
                    if (index >= _breeds.length) {
                      return Padding(
                        padding: EdgeInsets.all(Dimens.space16),
                        child: const Center(
                          child: CircularProgressIndicator.adaptive(),
                        ),
                      );
                    } else {
                      final breed = _breeds[index];
                      return InkWell(
                        child: _buildItem(breed),
                        onTap: () {
                        },
                      );
                    }
                  },
                );
              },
              failure: (message) => Center(child: AppEmpty(errorMessage: message)),
              empty: () => const Center(child: AppEmpty()),
            );
          },
        ),
      ),
    );
  }

  Widget _buildItem(BreedEntity breed) {
    return AppCard(
      heroTag: breed.id != null ? 'breed-${breed.id}' : null,
      margin: EdgeInsets.all(Dimens.space8),
      imageUrl: breed.imageUrl,
      titleText: breed.name,
      bodyText: breed.description,
      onPressed: () {
        context.pop(breed);
      },
    );
  }
}
