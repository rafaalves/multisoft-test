import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:multisoft_test/core/core.dart';

import '../../../domain/domain.dart';

part 'breeds_cubit.freezed.dart';
part 'breeds_state.dart';

class BreedsCubit extends Cubit<BreedsState> {
  final GetBreedsUsecase _usecase;

  BreedsCubit(this._usecase) : super(const _Loading());

  Future<void> fetch([BreedsParams params = const BreedsParams()]) async {
    /// Only show loading in 0 page
    await _fetchData(params);
  }

  Future<void> refresh(BreedsParams params) async {
    await _fetchData(params);
  }

  Future<void> _fetchData(BreedsParams params) async {
    if (params.page == 0) {
      emit(const _Loading());
    }

    final data = await _usecase.call(params);
    data.fold(
      (l) {
        if (l is ServerFailure) {
          if (params.page == 0) {
            emit(_Failure(l.message ?? ""));
          } else {
            emit(_Success([], l.message));
          }
        } else if (l is NoDataFailure) {
          if (params.page == 0) {
            emit(const _Empty());
          } else {
            emit(const _Success([]));
          }
        }
      },
      (r) {
        final items = r.data ?? [];
        emit(_Success(items));
      },
    );
  }
}
