part of 'breeds_cubit.dart';

@freezed
class BreedsState with _$BreedsState {
  const factory BreedsState.loading() = _Loading;
  const factory BreedsState.success(List<BreedEntity> data, [String? message]) = _Success;
  const factory BreedsState.failure(String message) = _Failure;
  const factory BreedsState.empty() = _Empty;
}
