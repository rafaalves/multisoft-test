import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../entities/entities.dart';
import '../repositories/repositories.dart';

class GetBreedsUsecase extends Usecase<BreedsListEntity, BreedsParams> {
  final IBreedsRepository _repo;

  GetBreedsUsecase(this._repo);

  @override
  Future<Either<IFailure, BreedsListEntity>> call(BreedsParams params) => _repo.getBreeds(params);
}
