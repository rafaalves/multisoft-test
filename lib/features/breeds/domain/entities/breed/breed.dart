import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'breed.freezed.dart';
part 'breed.g.dart';

@freezed
class BreedsListEntity with _$BreedsListEntity {
  const factory BreedsListEntity({
    List<BreedEntity>? data,
    int? currentPage,
    int? lastPage,
  }) = _BreedsListEntity;
}

@freezed
class BreedEntity extends Equatable with _$BreedEntity {
  const BreedEntity._();

  const factory BreedEntity({
    String? id,
    String? name,
    String? description,
    String? imageUrl,
  }) = _BreedEntity;

  factory BreedEntity.fromJson(Map<String, dynamic> json) => _$BreedEntityFromJson(json);

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        imageUrl,
      ];
}
