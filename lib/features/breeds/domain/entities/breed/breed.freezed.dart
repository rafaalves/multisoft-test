// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'breed.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BreedsListEntity {
  List<BreedEntity>? get data => throw _privateConstructorUsedError;
  int? get currentPage => throw _privateConstructorUsedError;
  int? get lastPage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $BreedsListEntityCopyWith<BreedsListEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BreedsListEntityCopyWith<$Res> {
  factory $BreedsListEntityCopyWith(
          BreedsListEntity value, $Res Function(BreedsListEntity) then) =
      _$BreedsListEntityCopyWithImpl<$Res, BreedsListEntity>;
  @useResult
  $Res call({List<BreedEntity>? data, int? currentPage, int? lastPage});
}

/// @nodoc
class _$BreedsListEntityCopyWithImpl<$Res, $Val extends BreedsListEntity>
    implements $BreedsListEntityCopyWith<$Res> {
  _$BreedsListEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
    Object? currentPage = freezed,
    Object? lastPage = freezed,
  }) {
    return _then(_value.copyWith(
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<BreedEntity>?,
      currentPage: freezed == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int?,
      lastPage: freezed == lastPage
          ? _value.lastPage
          : lastPage // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BreedsListEntityImplCopyWith<$Res>
    implements $BreedsListEntityCopyWith<$Res> {
  factory _$$BreedsListEntityImplCopyWith(_$BreedsListEntityImpl value,
          $Res Function(_$BreedsListEntityImpl) then) =
      __$$BreedsListEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<BreedEntity>? data, int? currentPage, int? lastPage});
}

/// @nodoc
class __$$BreedsListEntityImplCopyWithImpl<$Res>
    extends _$BreedsListEntityCopyWithImpl<$Res, _$BreedsListEntityImpl>
    implements _$$BreedsListEntityImplCopyWith<$Res> {
  __$$BreedsListEntityImplCopyWithImpl(_$BreedsListEntityImpl _value,
      $Res Function(_$BreedsListEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = freezed,
    Object? currentPage = freezed,
    Object? lastPage = freezed,
  }) {
    return _then(_$BreedsListEntityImpl(
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<BreedEntity>?,
      currentPage: freezed == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int?,
      lastPage: freezed == lastPage
          ? _value.lastPage
          : lastPage // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$BreedsListEntityImpl implements _BreedsListEntity {
  const _$BreedsListEntityImpl(
      {final List<BreedEntity>? data, this.currentPage, this.lastPage})
      : _data = data;

  final List<BreedEntity>? _data;
  @override
  List<BreedEntity>? get data {
    final value = _data;
    if (value == null) return null;
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final int? currentPage;
  @override
  final int? lastPage;

  @override
  String toString() {
    return 'BreedsListEntity(data: $data, currentPage: $currentPage, lastPage: $lastPage)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BreedsListEntityImpl &&
            const DeepCollectionEquality().equals(other._data, _data) &&
            (identical(other.currentPage, currentPage) ||
                other.currentPage == currentPage) &&
            (identical(other.lastPage, lastPage) ||
                other.lastPage == lastPage));
  }

  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_data), currentPage, lastPage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BreedsListEntityImplCopyWith<_$BreedsListEntityImpl> get copyWith =>
      __$$BreedsListEntityImplCopyWithImpl<_$BreedsListEntityImpl>(
          this, _$identity);
}

abstract class _BreedsListEntity implements BreedsListEntity {
  const factory _BreedsListEntity(
      {final List<BreedEntity>? data,
      final int? currentPage,
      final int? lastPage}) = _$BreedsListEntityImpl;

  @override
  List<BreedEntity>? get data;
  @override
  int? get currentPage;
  @override
  int? get lastPage;
  @override
  @JsonKey(ignore: true)
  _$$BreedsListEntityImplCopyWith<_$BreedsListEntityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

BreedEntity _$BreedEntityFromJson(Map<String, dynamic> json) {
  return _BreedEntity.fromJson(json);
}

/// @nodoc
mixin _$BreedEntity {
  String? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get imageUrl => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BreedEntityCopyWith<BreedEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BreedEntityCopyWith<$Res> {
  factory $BreedEntityCopyWith(
          BreedEntity value, $Res Function(BreedEntity) then) =
      _$BreedEntityCopyWithImpl<$Res, BreedEntity>;
  @useResult
  $Res call({String? id, String? name, String? description, String? imageUrl});
}

/// @nodoc
class _$BreedEntityCopyWithImpl<$Res, $Val extends BreedEntity>
    implements $BreedEntityCopyWith<$Res> {
  _$BreedEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? imageUrl = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      imageUrl: freezed == imageUrl
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BreedEntityImplCopyWith<$Res>
    implements $BreedEntityCopyWith<$Res> {
  factory _$$BreedEntityImplCopyWith(
          _$BreedEntityImpl value, $Res Function(_$BreedEntityImpl) then) =
      __$$BreedEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? id, String? name, String? description, String? imageUrl});
}

/// @nodoc
class __$$BreedEntityImplCopyWithImpl<$Res>
    extends _$BreedEntityCopyWithImpl<$Res, _$BreedEntityImpl>
    implements _$$BreedEntityImplCopyWith<$Res> {
  __$$BreedEntityImplCopyWithImpl(
      _$BreedEntityImpl _value, $Res Function(_$BreedEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? imageUrl = freezed,
  }) {
    return _then(_$BreedEntityImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      imageUrl: freezed == imageUrl
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$BreedEntityImpl extends _BreedEntity {
  const _$BreedEntityImpl({this.id, this.name, this.description, this.imageUrl})
      : super._();

  factory _$BreedEntityImpl.fromJson(Map<String, dynamic> json) =>
      _$$BreedEntityImplFromJson(json);

  @override
  final String? id;
  @override
  final String? name;
  @override
  final String? description;
  @override
  final String? imageUrl;

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BreedEntityImplCopyWith<_$BreedEntityImpl> get copyWith =>
      __$$BreedEntityImplCopyWithImpl<_$BreedEntityImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$BreedEntityImplToJson(
      this,
    );
  }
}

abstract class _BreedEntity extends BreedEntity {
  const factory _BreedEntity(
      {final String? id,
      final String? name,
      final String? description,
      final String? imageUrl}) = _$BreedEntityImpl;
  const _BreedEntity._() : super._();

  factory _BreedEntity.fromJson(Map<String, dynamic> json) =
      _$BreedEntityImpl.fromJson;

  @override
  String? get id;
  @override
  String? get name;
  @override
  String? get description;
  @override
  String? get imageUrl;
  @override
  @JsonKey(ignore: true)
  _$$BreedEntityImplCopyWith<_$BreedEntityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
