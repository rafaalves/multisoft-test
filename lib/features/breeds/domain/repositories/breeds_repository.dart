import 'package:dartz/dartz.dart';
import 'package:multisoft_test/core/core.dart';

import '../entities/entities.dart';

abstract class IBreedsRepository {
  Future<Either<IFailure, BreedsListEntity>> getBreeds(BreedsParams params);
}
