import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/dependencies_injection.dart';
import 'package:multisoft_test/features/pets/pets.dart';
import 'package:multisoft_test/utils/utils.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    log.d(const String.fromEnvironment('ENV'));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
    );

    /// Pass context to appRoute
    AppRouter.setStream(context);

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => sl<EditPetCubit>()),
        BlocProvider(create: (_) => sl<DeletePetCubit>()),
        BlocProvider(create: (_) => sl<ListPetCubit>()),
      ],
      child: AppToast.parent(
        child: MaterialApp.router(
          routerConfig: AppRouter.router,
          localizationsDelegates: const [
            Strings.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          debugShowCheckedModeBanner: false,
          builder: (BuildContext context, Widget? child) {
            final MediaQueryData data = MediaQuery.of(context);

            return MediaQuery(
              data: data.copyWith(
                textScaler: const TextScaler.linear(1),
                alwaysUse24HourFormat: true,
              ),
              child: child!,
            );
          },
          title: Constants.get.appName,
          theme: themeLight(context),
          darkTheme: themeDark(context),
          locale: Locale(Constants.defaultLanguage),
          supportedLocales: L10n.all,
          themeMode: ThemeMode.light,
        ),
      ),
    );
  }
}
