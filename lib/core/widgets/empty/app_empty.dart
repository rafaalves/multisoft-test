import 'package:flutter/material.dart';

import '../../localization/localization.dart';

class AppEmpty extends StatelessWidget {
  final String? errorMessage;

  const AppEmpty({
    super.key,
    this.errorMessage,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          errorMessage ?? Strings.of(context)!.errorNoData,
        ),
      ],
    );
  }
}
