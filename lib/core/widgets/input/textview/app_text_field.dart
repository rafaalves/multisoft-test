import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../localization/localization.dart';
import '../../../resources/resources.dart';
import '../input_theme.dart';

class AppTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? labelText;
  final String? placeholder;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final TextCapitalization? textCapitalization;
  final String? Function(String?)? onValidator;
  final void Function(String)? onChanged;
  final void Function(String)? onFieldSubmitted;
  final bool? obscureText;
  final dynamic prefixIcon;
  final dynamic suffixIcon;
  final List<TextInputFormatter>? inputFormatters;
  final VoidCallback? onTap;
  final bool? enabled;
  final AutovalidateMode? autovalidate;
  final int? maxLines;
  final bool required;
  final bool readOnly;
  final EdgeInsets? margin;
  final Iterable<String>? autofillHints;

  const AppTextField({
    super.key,
    this.controller,
    this.labelText,
    this.placeholder,
    this.keyboardType,
    this.onValidator,
    this.onChanged,
    this.obscureText,
    this.prefixIcon,
    this.suffixIcon,
    this.textInputAction,
    this.textCapitalization,
    this.onFieldSubmitted,
    this.inputFormatters,
    this.onTap,
    this.enabled,
    this.autovalidate,
    this.maxLines = 1,
    this.required = true,
    this.readOnly = false,
    this.margin,
    this.autofillHints,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ?? EdgeInsets.only(bottom: Dimens.space16),
      child: TextFormField(
        key: key,
        autovalidateMode: autovalidate ?? AutovalidateMode.onUserInteraction,
        autofillHints: autofillHints,
        controller: controller,
        keyboardType: keyboardType,
        textInputAction: textInputAction ?? TextInputAction.next,
        obscureText: obscureText ?? false,
        inputFormatters: inputFormatters,
        onTap: onTap,
        readOnly: readOnly || onTap != null,
        maxLines: maxLines,
        textCapitalization: textCapitalization ?? TextCapitalization.sentences,
        style: Theme.of(context).textTheme.bodyMedium,
        cursorColor: Palette.text,
        decoration: InputTheme.inputDecoration(
          context: context,
          enabled: enabled ?? true,
          labelText: labelText,
          placeholder: placeholder,
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
        ),
        validator: !required
            ? null
            : (onValidator ??
                (value) => (value?.isEmpty ?? true) ? Strings.of(context)!.errorEmptyField : null),
        onChanged: onChanged,
        onFieldSubmitted: onFieldSubmitted,
      ),
    );
  }
}
