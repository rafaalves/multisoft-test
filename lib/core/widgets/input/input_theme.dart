import 'package:flutter/material.dart';

import '../../resources/resources.dart';

class InputTheme {
  static InputDecoration inputDecoration({
    required BuildContext context,
    bool enabled = true,
    String? labelText,
    String? placeholder,
    dynamic suffixIcon,
    dynamic prefixIcon,
  }) {
    return InputDecoration(
      enabled: enabled,
      labelText: labelText,
      hintText: placeholder,
      suffixIcon: _buildIcon(context, suffixIcon),
      prefixIcon: _buildIcon(context, prefixIcon),
      floatingLabelBehavior: FloatingLabelBehavior.always,
      errorMaxLines: 2,
      filled: true,
      contentPadding: EdgeInsets.symmetric(
        vertical: Dimens.space8,
        horizontal: Dimens.space8,
      ),
      errorStyle: Theme.of(context).textTheme.labelSmall?.copyWith(
        color: Theme.of(context).extension<AppColors>()!.red,
      ),
    );
  }

  static Widget? _buildIcon(BuildContext context, dynamic icon) {
    if (icon == null) {
      return null;
    }

    if (icon.runtimeType == IconData) {
      return Icon(
        icon,
      );
    }

    if (icon.runtimeType == IconButton) {
      final iconButton = icon as IconButton;
      return IconButton(
        icon: Icon(
          (iconButton.icon as Icon).icon,
        ),
        onPressed: iconButton.onPressed,
      );
    }

    if (icon is! Widget) {
      return null;
    }

    return Material(
      color: Colors.transparent,
      child: icon,
    );
  }
}
