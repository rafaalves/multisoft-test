import 'package:flutter/material.dart';
import 'package:multisoft_test/core/core.dart';
import 'package:multisoft_test/utils/utils.dart';
import 'package:oktoast/oktoast.dart';

class AppToast extends StatelessWidget {
  static OKToast parent({required Widget child}) {
    return OKToast(child: child);
  }

  static ToastFuture? success(
    BuildContext context,
    String message,
  ) {
    return show(
      context,
      AppToast(
        bgColor: Theme.of(context).extension<AppColors>()!.green,
        icon: Icons.check_circle,
        message: message,
        textColor: Colors.white,
      ),
    );
  }

  static ToastFuture? error(
    BuildContext context,
    String message,
  ) {
    return show(
      context,
      AppToast(
        bgColor: Theme.of(context).extension<AppColors>()!.red,
        icon: Icons.cancel,
        message: message,
        textColor: Colors.white,
      ),
    );
  }

  static ToastFuture? alert(
    BuildContext context,
    String message,
  ) {
    return show(
      context,
      AppToast(
        bgColor: Theme.of(context).extension<AppColors>()!.yellow,
        icon: Icons.info,
        message: message,
        textColor: Colors.white,
      ),
    );
  }

  static ToastFuture? show(
    BuildContext context,
    AppToast toast,
  ) {
    try {
      dismissAllToast(showAnim: true);
      return showToastWidget(
        toast,
        dismissOtherToast: true,
        position: ToastPosition.bottom,
        duration: const Duration(seconds: 3),
      );
    } catch (e) {
      // FirebaseCrashLogger().nonFatalError(error: e, stackTrace: stackTrace);
      log.e("$e");
    }
    return null;
  }

  final IconData? icon;
  final Color? bgColor;
  final Color? textColor;
  final String? message;

  const AppToast({
    super.key,
    this.icon,
    this.bgColor,
    this.message,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(Dimens.cornerRadiusToast),
          ),
          padding: EdgeInsets.symmetric(
            vertical: Dimens.space8,
            horizontal: Dimens.space16,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                icon,
                color: textColor,
              ),
              SizedBox(width: Dimens.space4),
              Container(
                constraints: BoxConstraints(maxWidth: Dimens.toastWidth),
                child: Text(
                  message!,
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                        color: textColor,
                      ),
                  textAlign: TextAlign.center,
                  maxLines: 5,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
