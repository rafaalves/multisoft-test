import 'package:flutter/material.dart';

class AppTextBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final String? subtitle;
  final List<Widget>? actions;

  const AppTextBar({
    Key? key,
    this.title,
    this.subtitle,
    this.actions,
  }) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Column(
        children: [
          if (title?.isNotEmpty ?? false)
            Text(
              title!,
              style: Theme.of(context).textTheme.labelLarge,
            ),
          if (subtitle?.isNotEmpty ?? false)
            Text(
              subtitle!,
              style: Theme.of(context).textTheme.bodySmall?.merge(TextStyle(
                    color: Theme.of(context).appBarTheme.titleTextStyle?.color,
                  )),
            ),
        ],
      ),
      actions: actions,
    );
  }
}
