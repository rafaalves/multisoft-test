import 'package:flutter/material.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../resources/resources.dart';

class AppTextButton extends StatelessWidget {
  final String labelText;
  final double? width;
  final VoidCallback? onPressed;
  final Color? color;
  final EdgeInsets? padding;

  const AppTextButton({
    Key? key,
    required this.labelText,
    this.width,
    this.onPressed,
    this.color,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      constraints: BoxConstraints(minWidth: width ?? context.widthInPercent(100)),
      height: Dimens.buttonHeight,
      child: TextButton(
        onPressed: onPressed,
        style: TextButton.styleFrom(
          foregroundColor: color ?? Theme.of(context).extension<AppColors>()!.secondary,
          padding: EdgeInsets.symmetric(horizontal: Dimens.space24),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Dimens.cornerRadiusButton),
          ),
        ),
        child: Text(
          labelText,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.labelMedium?.copyWith(
            color: color ?? Theme.of(context).extension<AppColors>()!.buttonText,
          ),
        ),
      ),
    );
  }
}
