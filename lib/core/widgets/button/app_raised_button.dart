import 'package:flutter/material.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../resources/resources.dart';

class AppRaisedButton extends StatelessWidget {
  final double? width;
  final String? labelText;
  final Icon? icon;
  final VoidCallback? onPressed;
  final Color? color;
  final bool loading;

  const AppRaisedButton({
    Key? key,
    this.width,
    this.labelText,
    this.onPressed,
    this.icon,
    this.color,
    this.loading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: width ?? context.widthInPercent(100)),
      height: Dimens.buttonHeight,
      child: ElevatedButton(
        onPressed: loading ? null : onPressed,
        style: ElevatedButton.styleFrom(
          backgroundColor: color ?? Theme.of(context).extension<AppColors>()!.secondary,
          foregroundColor: Theme.of(context).extension<AppColors>()!.buttonText,
          padding: EdgeInsets.symmetric(horizontal: Dimens.space24),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Dimens.cornerRadiusButton),
          ),
        ),
        child: loading
            ? Container(
                width: Dimens.space24,
                height: Dimens.space24,
                alignment: Alignment.center,
                child: const CircularProgressIndicator.adaptive(),
              )
            : _buildChild(context),
      ),
    );
  }

  Widget _buildChild(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (icon != null) _buildWithIcon(icon!),
        if (labelText != null) _buildText(context, labelText!),
      ],
    );
  }

  _buildWithIcon(Icon icon) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        icon,
        SizedBox(width: Dimens.space4),
      ],
    );
  }

  Widget _buildText(BuildContext context, String labelText) {
    return Text(
      labelText.toUpperCase(),
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.labelMedium?.copyWith(
            color: Theme.of(context).extension<AppColors>()!.background,
            fontWeight: FontWeight.w700,
            letterSpacing: 1.0,
          ),
    );
  }
}
