import 'package:flutter/material.dart';

import '../../resources/resources.dart';
import '../widgets.dart';

class AppCard extends StatelessWidget {
  final String? heroTag;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final String? imageUrl;
  final String? titleText;
  final String? bodyText;
  final TextStyle? titleStyle;
  final VoidCallback? onPressed;

  const AppCard({
    Key? key,
    this.heroTag,
    this.margin,
    this.padding,
    this.imageUrl,
    this.titleText,
    this.bodyText,
    this.titleStyle,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Material(
        color: Theme.of(context).extension<AppColors>()!.card,
        borderRadius: BorderRadius.circular(Dimens.cornerCardRadius),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          onTap: onPressed,
          child: SizedBox(
            height: Dimens.space80 + ((padding?.top ?? Dimens.space12) + (padding?.bottom ?? Dimens.space12)),
            child: Stack(
              children: [
                Hero(
                  tag: '${heroTag ?? UniqueKey().toString()}-container',
                  child: const SizedBox(
                    width: double.infinity,
                    height: double.infinity,
                    // color: Theme.of(context).extension<AppColors>()!.card,
                  ),
                ),
                Container(
                  padding: padding ?? EdgeInsets.all(Dimens.space12),
                  child: Row(
                    children: [
                      _buildImage(),
                      Expanded(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _buildTitle(context),
                            if (bodyText != null)
                              Hero(
                                tag: '${heroTag ?? UniqueKey().toString()}-body',
                                child: Text(
                                  bodyText!,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: Theme.of(context).textTheme.bodySmall,
                                ),
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    if (titleText == null) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.only(
        bottom: Dimens.space4,
      ),
      child: Hero(
        tag: '${heroTag ?? UniqueKey().toString()}-title',
        child: Text(
          titleText!,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: titleStyle ??
              Theme.of(context).textTheme.titleSmall?.merge(const TextStyle(
                    fontWeight: FontWeight.bold,
                  )),
        ),
      ),
    );
  }

  Widget _buildImage() {
    if (imageUrl == null) {
      return Container();
    }

    return Container(
      margin: EdgeInsets.only(
        right: Dimens.space8,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(Dimens.cornerCardRadius)),
      ),
      clipBehavior: Clip.hardEdge,
      child: Hero(
        tag: '${heroTag ?? UniqueKey().toString()}-image',
        child: AppImage(
          size: Dimens.space80,
          url: imageUrl!,
        ),
      ),
    );
  }
}
