import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../loading/app_loading.dart';

class AppImage extends StatelessWidget {
  final String url;
  final double? size;

  const AppImage({
    super.key,
    required this.url,
    this.size,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      fit: BoxFit.cover,
      width: size,
      height: size,
      fadeInDuration: const Duration(milliseconds: 300),
      imageUrl: url,
      placeholder: (context, url) {
        return const SizedBox(
          child: AppLoading(showMessage: false),
        );
      },
      // errorWidget: (context, url, error) =>
      //     new SvgPicture.asset(Images.icEmpty),
    );
  }
}
