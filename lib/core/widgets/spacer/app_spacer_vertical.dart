import 'package:flutter/material.dart';

import '../../resources/resources.dart';

class AppSpacerVertical extends StatelessWidget {
  final double? value;

  const AppSpacerVertical({
    super.key,
    this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: value ?? Dimens.space8,
    );
  }
}
