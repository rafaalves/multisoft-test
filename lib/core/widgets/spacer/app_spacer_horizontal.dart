import 'package:flutter/material.dart';

import '../../resources/resources.dart';

class AppSpacerHorizontal extends StatelessWidget {
  final double? value;

  const AppSpacerHorizontal({
    super.key,
    this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: value ?? Dimens.space8,
    );
  }
}
