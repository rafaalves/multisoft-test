import 'package:flutter/material.dart';

import '../../localization/localization.dart';

class DialogModalButton {
  String labelText;
  VoidCallback onPressed;
  Color? color;

  factory DialogModalButton.cancel(BuildContext context) {
    return DialogModalButton(
      labelText: Strings.of(context)!.cancel,
      onPressed: Navigator.of(context).pop,
    );
  }

  factory DialogModalButton.close(BuildContext context) {
    return DialogModalButton(
      labelText: Strings.of(context)!.close,
      onPressed: Navigator.of(context).pop,
    );
  }

  DialogModalButton({
    required this.labelText,
    required this.onPressed,
    this.color,
  });
}
