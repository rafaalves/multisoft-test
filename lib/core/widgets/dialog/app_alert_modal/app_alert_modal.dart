import 'package:flutter/material.dart';

import '../../../resources/resources.dart';
import '../../button/button.dart';
import '../app_modal_button.dart';

class AppAlertModal extends StatelessWidget {
  static open({
    required BuildContext context,
    required Widget content,
    String? title,
    List<DialogModalButton> Function(BuildContext context)? actions,
    Color? barrierColor,
  }) {
    return showDialog(
      context: context,
      barrierColor: barrierColor,
      builder: (BuildContext context) {
        return AppAlertModal(
          title: title,
          content: content,
          actions: actions,
        );
      },
    );
  }

  final String? title;
  final Widget content;
  final List<DialogModalButton> Function(BuildContext context)? actions;

  const AppAlertModal({
    Key? key,
    this.title,
    this.actions,
    required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(Dimens.cornerRadius)),
      ),
      titlePadding: EdgeInsets.fromLTRB(
        Dimens.space24,
        Dimens.space24,
        Dimens.space24,
        Dimens.zero,
      ),
      actionsPadding: EdgeInsets.fromLTRB(
        Dimens.space16,
        Dimens.zero,
        Dimens.space16,
        Dimens.space8,
      ),
      contentPadding: EdgeInsets.fromLTRB(
        Dimens.space24,
        Dimens.space16,
        Dimens.space24,
        Dimens.space24,
      ),
      actionsOverflowButtonSpacing: Dimens.space8,
      title: title == null
          ? null
          : Text(
              title!,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
      content: content,
      actionsOverflowDirection: VerticalDirection.up,
      actions: (actions?.call(context) ?? [DialogModalButton.close(context)])
          .map((e) => _buildActionButton(e))
          .toList(),
    );
  }

  Widget _buildActionButton(DialogModalButton button) {
    return AppTextButton(
      width: Dimens.zero,
      labelText: button.labelText,
      onPressed: button.onPressed,
      color: button.color,
    );
  }
}
