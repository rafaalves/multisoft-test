import 'package:flutter/material.dart';

import '../../../localization/localization.dart';
import '../../../resources/resources.dart';
import '../app_modal_button.dart';
import 'app_alert_modal.dart';

class AppAlert {
  static Future<void> showDanger({
    required BuildContext context,
    required String description,
    required VoidCallback onPressed,
    String? title,
    String? positiveLabelButton,
  }) {
    return AppAlertModal.open(
      context: context,
      title: title,
      // barrierColor: Theme.of(context).extension<AppColors>()!.red!.withOpacity(0.1),
      content: Text(
        description,
        style: Theme.of(context).textTheme.bodyMedium,
      ),
      actions: (_) => [
        DialogModalButton.cancel(_),
        DialogModalButton(
          labelText: positiveLabelButton ?? Strings.of(_)!.continueLabel,
          color: Theme.of(_).extension<AppColors>()!.red,
          onPressed: () async {
            Navigator.of(_).pop();
            onPressed.call();
          },
        ),
      ],
    );
  }
}
