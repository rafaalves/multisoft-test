import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:multisoft_test/dependencies_injection.dart';
import 'package:multisoft_test/features/breeds/breeds.dart';
import 'package:multisoft_test/features/pets/pets.dart';

export 'package:go_router/go_router.dart';

enum AppRoutes {
  root("/"),

  // Breeds page
  breedList("/breed/list"),

  //Pets pages
  petView("/pet/view"),
  petEdit("/pet/edit"),
  petList("/pet/list"),
  ;

  const AppRoutes(this.path);

  final String path;
}

class AppRouter {
  static late BuildContext context;

  AppRouter.setStream(BuildContext ctx) {
    context = ctx;
  }

  static final GoRouter router = GoRouter(
    routes: [
      GoRoute(
        path: AppRoutes.root.path,
        name: AppRoutes.root.name,
        redirect: (_, __) => AppRoutes.petList.path,
      ),
      GoRoute(
        path: AppRoutes.breedList.path,
        name: AppRoutes.breedList.name,
        builder: (_, __) => BlocProvider(
          create: (_) => sl<BreedsCubit>()..fetch(),
          child: const BreedListPage(),
        ),
      ),
      GoRoute(
        path: AppRoutes.petView.path,
        name: AppRoutes.petView.name,
        builder: (_, __) => PetViewPage(
          pet: PetEntity.fromJson(__.extra as Map<String, dynamic>? ?? {}),
        ),
      ),
      GoRoute(
        path: AppRoutes.petEdit.path,
        name: AppRoutes.petEdit.name,
        builder: (_, __) => PetEditPage(
          pet: __.extra != null ? PetEntity.fromJson(__.extra as Map<String, dynamic>) : null,
        ),
      ),
      GoRoute(
        path: AppRoutes.petList.path,
        name: AppRoutes.petList.name,
        builder: (_, __) => const PetListPage(),
      ),
    ],
    initialLocation: AppRoutes.root.path,
    routerNeglect: true,
    debugLogDiagnostics: kDebugMode,
  );
}
