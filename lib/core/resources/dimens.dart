extension SizeExtension on num {
  double get sp => toDouble();
  double get w => toDouble();
  double get r => toDouble();
}

class Dimens {
  Dimens._();

  static double displayLarge = 96.sp;
  static double displayMedium = 60.sp;
  static double displaySmall = 48.sp;
  static double headlineMedium = 34.sp;
  static double headlineSmall = 24.sp;
  static double titleLarge = 20.sp;
  static double bodyLarge = 16.sp;
  static double bodyMedium = 14.sp;
  static double bodySmall = 12.sp;
  static double titleMedium = 18.sp;
  static double titleSmall = 14.sp;
  static double labelLarge = 16.sp;
  static double labelMedium = 14.sp;
  static double labelSmall = 10.sp;

  static double zero = 0;
  static double space2 = 2.w;
  static double space3 = 3.w;
  static double space4 = 4.w;
  static double space6 = 6.w;
  static double space8 = 8.w;
  static double space12 = 12.w;
  static double space16 = 16.w;
  static double space24 = 24.w;
  static double space30 = 30.w;
  static double space32 = 32.w;
  static double space36 = 36.w;
  static double space40 = 40.w;
  static double space46 = 46.w;
  static double space50 = 50.w;
  static double space80 = 80.w;

  static double formWidth = 360.w;
  static double profilePictureSize = 200.w;
  static double toastWidth = 250.w;
  static double buttonHeight = 40.w;

  static double cornerRadius = 8.r;
  static double cornerCardRadius = 8.r;
  static double cornerRadiusButton = 4.r;
  static double cornerRadiusToast = 16.r;
}
