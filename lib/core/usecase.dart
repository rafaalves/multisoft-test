import 'package:dartz/dartz.dart';

import 'errors/errors.dart';

// TODO: Put the I ahead usecase word
abstract class Usecase<Type, Params> {
  Future<Either<IFailure, Type>> call(Params params);
}

// TODO: remove from this file
/// Class to handle when useCase don't need params
class NoParams {}
