import 'package:flutter/material.dart';
import 'package:multisoft_test/utils/utils.dart';

class L10n {
  L10n._();

  static final all = [
    Locale(Constants.englishAbbreviated),
    Locale(Constants.portugueseAbbreviated),
  ];

  static String getFlag(String code) {
    if (code == Constants.portugueseAbbreviated) {
      return Constants.portuguese;
    }
    return Constants.english;
  }
}
