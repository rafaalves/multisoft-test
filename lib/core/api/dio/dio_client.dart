import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:multisoft_test/utils/utils.dart';

import '../../errors/errors.dart';
import '../api_path.dart';
import '../isolate_parser.dart';
import 'dio_interceptor.dart';

int _timeout = 5000;

typedef ResponseConverter<T> = T Function(dynamic response);

class ApiClient with MainBoxMixin {
  String baseUrl = ApiPath.baseUrl;

  String? _token;
  bool _isUnitTest = false;
  late Dio _dio;

  ApiClient({bool isUnitTest = false}) {
    _isUnitTest = isUnitTest;

    try {
      _token = getData(MainBoxKeys.token);
    } catch (_) {}

    _dio = _createDio();

    if (!_isUnitTest) {
      _dio.interceptors.add(DioInterceptor());
    }
  }

  Dio client({String? baseUrlLocal}) {
    if (_isUnitTest) {
      /// Return static dio if is unit test
      return _dio;
    } else {
      /// We need to recreate dio to avoid token issue after login
      try {
        _token = getData(MainBoxKeys.token);
      } catch (_) {}
      final dio = _createDio(
        baseUrlLocal: baseUrlLocal,
      );

      if (!_isUnitTest) {
        dio.interceptors.add(DioInterceptor());
      }

      return dio;
    }
  }

  Dio _createDio({String? baseUrlLocal}) {
    return Dio(
      BaseOptions(
        baseUrl: baseUrlLocal ?? baseUrl,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          if (_token != null) ...{
            'Authorization': _token,
          },
        },
        connectTimeout: Duration(milliseconds: _timeout),
        receiveTimeout: Duration(milliseconds: _timeout),
        validateStatus: (int? status) {
          return status! > 0;
        },
      ),
    );
  }

  Future<Either<IFailure, T>> getRequest<T>(
    String url, {
    Map<String, dynamic>? queryParameters,
    required ResponseConverter<T> converter,
    bool isIsolate = true,
    String? baseUrl,
  }) async {
    try {
      final response = await client(
        baseUrlLocal: baseUrl,
      ).get(url, queryParameters: queryParameters);
      final result = await _response<T>(response, converter, isIsolate);
      return result;
    } on DioException catch (e) {
      return Left(_dioException(e));
    } catch (e) {
      return _generalException(e);
    }
  }

  Future<Either<IFailure, T>> postRequest<T>(
    String url, {
    Map<String, dynamic>? data,
    required ResponseConverter<T> converter,
    bool isIsolate = true,
    String? baseUrl,
  }) async {
    try {
      final response = await client(
        baseUrlLocal: baseUrl,
      ).post(url, data: data);
      final result = await _response<T>(response, converter, isIsolate);
      return result;
    } on DioException catch (e) {
      return Left(_dioException(e));
    } catch (e) {
      return _generalException(e);
    }
  }

  Future<Either<IFailure, T>> _response<T>(
    Response response,
    ResponseConverter<T> converter,
    bool isIsolate,
  ) async {
    if ((response.statusCode ?? 0) < 200 || (response.statusCode ?? 0) > 201) {
      throw DioException(
        requestOptions: response.requestOptions,
        response: response,
      );
    }

    if (!isIsolate) {
      return Right(converter(response.data));
    }

    final isolateParse = response.data is List
        ? IsolateParser<T>.fromList(
            response.data as List<dynamic>,
            converter,
          )
        : IsolateParser<T>(
            response.data as Map<String, dynamic>,
            converter,
          );
    final result = await isolateParse.parseInBackground();
    return Right(result);
  }

  ServerFailure _dioException(DioException e) {
    if (!_isUnitTest) {
      // nonFatalError(error: e, stackTrace: stackTrace);
    }

    if (e.response?.data is Map) {
      final data = e.response!.data;
      return ServerFailure(
        title: data['titulo'],
        message: data['mensagem'],
      );
    }

    if (e.response?.statusCode == 500) {
      return const ServerFailure(
        message: 'Erro interno de servidor. Por favor, tente mais tarde.',
      );
    }

    if (e.response?.data is String) {
      return ServerFailure(
        message: e.response?.data,
      );
    }

    return const ServerFailure(
      message: 'Não foi possível completar sua requisição',
    );
  }

  _generalException(Object e) {
    return const Left(
      ServerFailure(
        message: 'Não foi possível completar sua requisição',
      ),
    );
  }
}
