import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class DioInterceptor extends Interceptor {
  static bool get debug => true;

  final BuildContext? context;
  final String? defaultMessageError;

  DioInterceptor({
    this.context,
    this.defaultMessageError,
  });

  @override
  onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    return handler.next(options);
  }

  @override
  onResponse(
    Response response,
    ResponseInterceptorHandler handler,
  ) async {
    _debugPrint(response);
    return handler.next(response);
  }

  void _debugPrint(
    Response? response, [
    bool error = false,
  ]) {
    if (response == null) {
      return;
    }
    if (debug) {
      String result = '${response.requestOptions.method}:'
          '${response.statusCode} '
          '${error ? ': ERROR' : ''} '
          ':::'
          ' ${response.requestOptions.baseUrl}'
          '${response.requestOptions.path}'
          '${response.requestOptions.method == 'GET' ? '' : '\n${jsonEncode(response.requestOptions.data ?? {})}'}'
          '${_dataResponse(response)}';
      log(result);
    }
  }

  String _dataResponse(Response response) {
    if (response.data is Map) {
      return '\n${jsonEncode(response.data ?? {})}';
    } else if (response.data is List) {
      return '\n${jsonEncode(response.data ?? [])}';
    }
    return '';
  }

  @override
  onError(
    DioException err,
    ErrorInterceptorHandler handler,
  ) async {
    _debugPrint(err.response, true);
    return super.onError(err, handler);
  }
}
