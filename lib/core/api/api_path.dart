class ApiPath {
  static const String baseUrl = '';
  static const String breedBaseUrl = 'https://api.thecatapi.com/v1/';
  static const String breedBaseImageUrl = 'https://cdn2.thecatapi.com/images/{id}.jpg';

  static const String breedsList = 'breeds';
}
