import 'failure.dart';

class ServerFailure extends IFailure {
  final String? title;
  final String? message;

  const ServerFailure({this.title, this.message});

  @override
  List<Object?> get props => [
        title,
        message,
      ];
}
